import lunr from 'lunr';
import { XMLParser } from 'fast-xml-parser';

class Searcher {
	constructor(app) {
		this._app = app;
		this.index = undefined; // the lunr index
		lunr.tokenizer = tagTokenizer;
		lunr.QueryLexer.termSeparator = /[\s\-]+/; // https://github.com/olivernn/lunr.js/issues/493
	}

	reset() {
		this.index = undefined;
	}

	/**
	 * 
	 * @param {Array} docModels An array of Document instances
	 * @returns 
	 */
	async indexDocuments(docModels) {
		const me = this;
		
		const config = async function() {
			this.ref('docId');
			this.field('index');
			this.field('text');

			this.metadataWhitelist = ['index', 'type', 'text'];

			const getDmPromise = function(docModel) {
				return new Promise(resolve => {
					this.add({
						docId: docModel.id,
						text: docModel.xmlString
					});
					setTimeout(() => {
						resolve();
					}, 0);
				});
			}

			for (const docModel of docModels) {
				me._app.publish('indexingDocument', docModel);
				await getDmPromise.call(this, docModel);
			}
		};
		
		// TODO lang detection/support
		
		const builder = new lunr.Builder;
		builder.pipeline.add(
			lunr.trimmer,
			// lunr.stopWordFilter,
			lunr.stemmer
		);
		builder.searchPipeline.add(
			lunr.stemmer
		);
		if (this._app.isDebug) console.time('callBuilder');
		await config.call(builder, builder);
		if (this._app.isDebug) console.timeEnd('callBuilder');

		if (this._app.isDebug) console.time('builderBuild');
		this.index = builder.build();
		if (this._app.isDebug) console.timeEnd('builderBuild');

		if (this._app.isDebug) console.time('getXML');

		const parser = new DOMParser();
		for (const docModel of docModels) {
			me._app.publish('processingDocument', docModel);
			const data = await this.getProcessedXML(docModel.id);
			docModel.xml = parser.parseFromString(data.xmlStr, 'application/xml');
			docModel.termsCount = data.termsCount;
			docModel.tokensCount = data.tokensCount;
		}

		if (this._app.isDebug) console.timeEnd('getXML');
	}

	async search(query, docId) {
		if (this._app.isDebug) console.time('search');

		if (Array.isArray(query)) {
			query = query[0];
		}

		const lunrResult = this.index.search(query);
		
		let result = [];
		lunrResult.forEach((doc) => {
			if (docId === undefined || doc.ref === docId) {
				const hits = doc.matchData.metadata;
				let matches = [];
				for (const hit in hits) {
					const entries = hits[hit]['text'];
					const tokenIndexes = entries['index'];
					tokenIndexes.forEach((tokenIndex, i) => {
						const type = entries['type'][i];
						if (type === 'term') {
							matches.push({
								index: tokenIndex,
								term: entries['text'][i]
							})
						}
					});
					
				}
				matches.sort((a, b) => {
					return a.index - b.index;
				});
				result.push({
					docId: doc.ref,
					matches: matches
				});
			}
		}, this);

		result.sort((a, b) => {
			return a.docIndex - b.docIndex;
		});

		if (this._app.isDebug) console.timeEnd('search');

		return result;
	}

	getProcessedXML(docId) {
		return new Promise(resolve => {
			const lunrResult = this.index.search('*');
			const tokens = [];
			lunrResult.forEach((doc) => {
				if (doc.ref === docId) {
					const hits = doc.matchData.metadata;
					for (const hit in hits) {
						const entries = hits[hit]['text'];
						const tokenIndexes = entries['index'];
						tokenIndexes.forEach((tokenIndex, i) => {
							const token = { index: tokenIndex };
							const tokenId = tokenIndex;
							const type = entries['type'][i];
							let text = entries['text'][i];
							if (type === 'term') {
								token.type = 'term';
								token.text = '<span tokenId="word_' + tokenId + '">' + text + '</span>';
							} else if (type.indexOf('text_') === 0) {
								token.type = type;
								token.text = text;
							} else {
								if (type === 'tagOpen') {
									if (tokenIndex === 0) {
										text += ' docId="'+docId+'"';
									}
									text += ' tokenId="tag_' + tokenId + '"';
								}
								token.type = 'tag';
								token.text = '<' + (type === 'tagClose' ? '/' : '') + text + '>';
							}
							tokens.push(token);
						})
					}
				}
			}, this);
			tokens.sort((a, b) => {
				return a.index - b.index;
			});
			const xmlStr = tokens.map(token => token.text).join('');

			let tokensCount = 0;
			let termsCount = 0;
			tokens.forEach((token) => {
				if (token.type === 'term') termsCount++;
				tokensCount++;
			});
			
			setTimeout(() => {
				resolve({docId, xmlStr, termsCount, tokensCount});
			}, 0);
		});
	}
}

function tagTokenizer(obj, metadata) {
	if (obj == null || obj == undefined) {
		return [];
	}

	if (Array.isArray(obj)) {
		throw new Error('Array??');
	}

	const str = obj.toString();//.toLowerCase();

	// https://github.com/NaturalIntelligence/fast-xml-parser/blob/HEAD/docs/v4/2.XMLparseOptions.md
	const parser = new XMLParser({
		preserveOrder: true,
		trimValues: false, // whitespace handling
		processEntities: true,
		ignoreAttributes: false,
		ignorePiTags: true,
		removeNSPrefix: false // preserve namespaces (needed for TEI's xml:id)
	});
	const xmlObj = parser.parse(str);

	const data = {
		tokens: [],
		tagStack: [],
		currNode: xmlObj
	}
	processNode(data);

	// console.log(data.tokens);

	return data.tokens;
}

function processNode(data) {
	if (Array.isArray(data.currNode)) {
		data.currNode.forEach((childNode) => {
			data.currNode = childNode;
			processNode(data);
		}, this);

		const tagName = data.tagStack.pop();
		if (tagName !== undefined) {
			data.tokens.push(new lunr.Token(tagName, {
				index: data.tokens.length,
				type: 'tagClose',
				text: tagName
			}));
		}
	}
	else if (data.currNode['#text'] !== undefined) {
		let textTokens = textTokenizer(data.currNode['#text'].toString(), data.tokens.length);
		textTokens.forEach((textToken) => {
			data.tokens.push(textToken);
		});
	} else {
		let tagName = '';
		let attributeString = '';
		for (const prop in data.currNode) {
			if (prop === ':@') {
				let attributes = [''];
				for (const attributeName in data.currNode[':@']) {
					attributes.push(attributeName.replace('@_', '') + '="' + data.currNode[':@'][attributeName] + '"');
				}
				attributeString = attributes.join(' ');
			} else {
				tagName = prop;
			}
		}

		data.tokens.push(new lunr.Token(tagName, {
			index: data.tokens.length,
			type: 'tagOpen',
			text: tagName+attributeString
		}));

		data.tagStack.push(tagName);
		data.currNode = data.currNode[tagName];
		
		processNode(data);
	}
}

function textTokenizer(str, indexStart) {
	let tokens = [];
	const len = str.length;

	function addTermToken(term) {
		if (term.length > 0) {
			tokens.push(new lunr.Token(term.toLowerCase(), {
				index: indexStart+tokens.length,
				type: 'term',
				text: term
			}));
		}
	}

	for (let sliceEnd = 0, sliceStart = 0; sliceEnd <= len; sliceEnd++) {
		let char = str.charAt(sliceEnd);
		const sliceLength = sliceEnd - sliceStart;

		if ((char.match(/[\p{Separator}]/u))) {
			if (sliceLength > 0) addTermToken(str.slice(sliceStart, sliceEnd));
			// TODO handle types of spacing
			const charCode = char.charCodeAt(0);
			if (charCode !== 32) {
				console.log(char.charCodeAt(0));
			} else {
			}
			tokens.push(new lunr.Token('$$$sepr$$$', {
				index: indexStart+tokens.length,
				type: 'text_separator',
				text: char
			}));

			sliceStart = sliceEnd + 1
		} else if ((char.match(/[\p{Punctuation}]/u))) {
			if (sliceLength > 0) addTermToken(str.slice(sliceStart, sliceEnd));

			if (char === '&') {
				char = '&amp;';
			}

			tokens.push(new lunr.Token('$$$punc$$$', {
				index: indexStart+tokens.length,
				type: 'text_punctuation',
				text: char
			}));

			sliceStart = sliceEnd + 1
		} else if ((char.match(/[\p{Symbol}]/u))) {
			if (sliceLength > 0) addTermToken(str.slice(sliceStart, sliceEnd));

			if (char === '<') {
				char = '&lt;';
			} else if (char === '>') {
				char = '&gt;';
			}

			tokens.push(new lunr.Token('$$$symb$$$', {
				index: indexStart+tokens.length,
				type: 'text_symbol',
				text: char
			
			}));
			sliceStart = sliceEnd + 1
		} else {
			if (sliceEnd === len) {
				if (sliceLength > 0) addTermToken(str.slice(sliceStart, sliceEnd));
			}
		}

	}
	return tokens;
}

export default Searcher;