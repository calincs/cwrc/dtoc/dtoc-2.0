import DocLoader from "./docloader";

export default Ext.define('DToC.InputDialog', {
	extend: 'Ext.window.Window',
	title: 'DToC Input Configuration',
	modal: true,
	maximized: true,
	closable: false,
	closeAction: 'hide',
	resizable: false,
	scrollable: 'y',
	bodyPadding: 10,
	inputCallback: undefined,
	statics: {
		i18n: {
		}
	},
	constructor: function(config) {
		this.inputCallback = config.inputCallback;

		Ext.apply(config, {
			layout: {
				type: 'vbox',
				align: 'middle',
				pack: 'start'
			},
			items: {
				xtype: 'form',
				fieldDefaults: {
					labelAlign: 'right',
					labelWidth: 125,
					width: 550
				},
				items: [{
					defaults: {
						width: 600,
						margin: '5px 0',
						padding: '10px',
					},
					items: [{
						xtype: 'fieldset',
						title: 'DToC Edition Metadata',
						defaultType: 'textfield',
						items: [{
							fieldLabel: 'Title',
							name: 'editionTitle',
							emptyText: ''
						},{
							fieldLabel: 'Subtitle',
							name: 'editionSubtitle',
							emptyText: ''
						}]
					},{
						xtype: 'fieldset',
						title: 'Documents',
						items: [{
							xtype: 'textareafield',
							fieldLabel: 'URLs',
							name: 'urls',
							emptyText: 'One or more URLs separated by new lines'
						},{
							hidden: true,
							html: '<p style="text-align: center; margin: 5px 0">or</p>'
						},{
							hidden: true,
							xtype: 'dtocfilefield',
							name: 'files',
							fieldLabel: 'Files',
							listeners: {
								render: function(filefield) {
									filefield.fileInputEl.dom.setAttribute('multiple', true);
								}
							}
						}]
					},{
						xtype: 'fieldset',
						title: 'Corpus Parts',
						defaultType: 'textfield',
						items: [{
							fieldLabel: 'Ignore default namespace (for simpler XPaths)',
							labelWidth: 300,
							name: 'ignoreNamespace',
							xtype: 'checkbox',
							checked: true,
							inputValue: true
						},{
							fieldLabel: 'Corpus Part',
							name: 'documents',
							emptyText: '//div[@type=\'chapter\']',
							validator: this.validateXPath
						},{
							fieldLabel: 'Part Content',
							name: 'documentContent',
							emptyText: '',
							validator: this.validateXPath
						},{
							fieldLabel: 'Part Title',
							name: 'documentTitle',
							emptyText: 'head/title',
							validator: this.validateXPath
						},{
							fieldLabel: 'Part Author',
							name: 'documentAuthor',
							emptyText: 'docAuthor[1]',
							validator: this.validateXPath
						},{
							fieldLabel: 'Corpus Index',
							name: 'indexDocument',
							emptyText: '//div[@type=\'index\']',
							validator: this.validateXPath
						}]
					},{
						xtype: 'fieldset',
						title: 'Formatting',
						defaultType: 'textfield',
						items: [{
							fieldLabel: 'Figures',
							name: 'documentImages',
							emptyText: '//graphic[@url]',
							validator: this.validateXPath
						},{
							fieldLabel: 'Notes',
							name: 'documentNotes',
							emptyText: '//note',
							validator: this.validateXPath
						},{
							fieldLabel: 'Links',
							name: 'documentLinks',
							emptyText: '//ref[starts-with(@target,\'http\')]',
							validator: this.validateXPath
						}]
					},{
						xtype: 'fieldset',
						title: 'Curation',
						items: [{
							xtype: 'textareafield',
							fieldLabel: 'Markup',
							name: 'markup',
							emptyText: '',
							validator: this.validateJSON
						},{
							xtype: 'textfield',
							name: 'toc',
							hidden: true
						}]
					}]
				}]
			},
			buttonAlign: 'center',
			buttons: [{
				text: 'Sample Values',
				glyph: 'xf0f6@FontAwesome',
				handler: function(btn) {
					const baseUrl = window.location.toString();
					fetch(baseUrl+'data/json/inke_curation.json', {})
						.then(response => response.ok ? response.json() : {markup: [], toc: []})
						.then(curation => {
							const form = btn.up('window').down('form').getForm();
							form.setValues({
								editionTitle: 'Sample Edition Title',
								editionSubtitle: '',
								urls: baseUrl+'data/xml/sample.xml',
								documents: '//div[@type="part"]',
								documentContent: 'div[@type="chapter"]',
								indexDocument: '//div[@type="index"]',
								documentTitle: 'head/title',
								documentAuthor: 'docAuthor[1]',
								documentImages: '//graphic[@url]',
								documentNotes: '//note',
								documentLinks: '//ref[starts-with(@target,"http")]',
								markup: JSON.stringify(curation.markup),
								toc: JSON.stringify(curation.toc)
							});
						});
				}
			},{xtype: 'tbspacer', width: 300},{
				text: 'Cancel',
				itemId: 'cancel',
				glyph: 'xf00d@FontAwesome',
				handler: function() {
					this.close();
				},
				scope: this
			},{
				text: 'Done',
				glyph: 'xf00c@FontAwesome',
				handler: function() {
					this.submitForm();
				},
				scope: this
			}]
		});

		this.callParent(arguments);
	},

	validateXPath: function(xpath) {
		if (xpath === '') return true;
		try {
			DocLoader.evaluateXPath(xpath, document, true);
		} catch (e) {
			const errorMessage = e.message.replace(/(token )?<eof>/, 'end of XPath');
			const errorMatch = errorMessage.match(/XError:(.*);/);
			if (errorMatch.length === 2) {
				return errorMatch[1]
			} else {
				return errorMessage;
			}
		}
		return true;
	},

	validateJSON: function(json) {
		if (json === '') return true;
		try {
			JSON.parse(json);
		} catch (e) {
			return e.message;
		}
		return true;
	},

	show: function() {
		this.down('#cancel').hide();
		this.callParent(arguments);
	},

	populateForm: function(data) {
		if (data.inputs) {
			if (typeof data.inputs[0] === 'string') {
				data.urls = data.inputs.join("\n");
			} else {
				console.warn('Cannot load Files into form!');
			}
		}
		if (data.curation && typeof data.curation === 'object') {
			data.markup = JSON.stringify(data.curation.markup);
			data.toc = JSON.stringify(data.curation.toc);
		}
		this.down('form').getForm().setValues(data);
		this.down('#cancel').show();
	},

	submitForm: function() {
		const form = this.down('form');
		if (form.isValid()) {
			const values = form.getValues();
			
			const files = form.getForm().findField('files').fileInputEl.dom.files;
			if (files.length > 0) {
				values.inputs = files;
			} else {
				const urls = values.urls.split(/\n/);
				values.inputs = urls;
			}
			delete values.urls;

			values.curation = {markup: [], toc: []};
			if (values.markup !== '') {
				values.curation.markup = JSON.parse(values.markup);
			}
			if (values.toc !== '') {
				values.curation.toc = JSON.parse(values.toc);
			}
			delete values.markup;
			delete values.toc;
			
			this.inputCallback.call(this, values);
		}
	}
});

Ext.define('DToC.form.field.File', {
	extend: 'Ext.form.field.File',
	alias: 'widget.dtocfilefield',
	onFileChange: function(button, e, value) {
		// remove fakepath
		var newValue = value.replace(/^.*(\\|\/|\:)/, ''); // TODO test in safari etc

		return this.callParent([ button, e, newValue ]);
	}
});