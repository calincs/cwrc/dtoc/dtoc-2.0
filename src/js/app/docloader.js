import Corpus from '../models/corpus.js';
import Document from '../models/document.js';


const xpath2FunctionNames = ["abs","adjust-dateTime-to-timezone","adjust-date-to-timezone","adjust-time-to-timezone","avg","base-uri","boolean","codepoints-to-string","collection","compare","context-item","current-date","current-dateTime","current-time","data","day-from-date","day-from-dateTime","days-from-dayTimeDuration","deep-equal","default-collation","distinct-nodes","distinct-values","doc","document-uri","empty","ends-with","error","escape-uri","exactly-one","exists","expanded-QName","false","hours-from-dateTime","hours-from-dayTimeDuration","hours-from-time","id","idref","implicit-timezone","index-of","input","in-scope-prefixes","insert-before","item-at","lang","last","local-name-from-QName","lower-case","matches","max","min","minutes-from-dateTime","minutes-from-dayTimeDuration","minutes-from-time","month-from-date","month-from-dateTime","months-from-yearMonthDuration","namespace-uri-for-prefix","namespace-uri-from-QName","node-kind","node-name","normalize-unicode","not","number","one-or-more","position","remove","replace","resolve-QName","resolve-uri","reverse","root","round-half-to-even","seconds-from-dateTime","seconds-from-dayTimeDuration","seconds-from-time","sequence-node-identical","string","string-join","string-pad","string-to-codepoints","subsequence","subtract-dateTimes-yielding-dayTimeDuration","subtract-dateTimes-yielding-yearMonthDuration","timezone-from-date","timezone-from-dateTime","timezone-from-time","tokenize","trace","true","unordered","upper-case","year-from-date","year-from-dateTime","years-from-yearMonthDuration","zero-or-one"];

export default class DocLoader {
	
	/**
	 * Input agnostic loading method. Creates a DToC.Corpus and sets it on DToC.App.
	 * @param {Object} config Configuration options
	 * @param {Function} config.callback A function that gets called for status updates, including the resulting Corpus.
	 * @param {Array} config.inputs An array of URLs or Files
	 * @param {String} [config.editionTitle] The title of the edition
	 * @param {String} [config.editionSubtitle] The subtitle of the edition
	 * @param {String} [config.indexDocument] The XPath matching the document to use as the corpus index
	 * @param {String} config.documents The XPath for locating the document(s) within each XML file
	 * @param {String} config.documentContent The XPath for locating the document content
	 * @param {String} config.documentTitle The XPath for locating the document title
	 * @param {String} config.documentAuthor The XPath for locating the document author
	 * @param {String} config.documentImages The XPath for identifying images
	 * @param {String} config.documentNotes The XPath for identifying notes
	 * @param {String} config.documentLinks The XPath for identifying links
	 * @param {Boolean} config.ignoreNamespace Whether to remove the root/default namespace prior to evaluating XPaths. Use to avoid having to use local-name() in XPaths.
	 * @param {Object} [config.curation] An optional curation object. Can be used to customize the tags panel and the table of contents panel.
	 * @param {Array} [config.curation.markup] An array of markup
	 * @param {Array} [config.curation.toc] The table of contents
	 */
	static loadCorpusFromInputs(config) {
		config.callback.call(this, {msg: 'Loading documents'});

		var inputs = config.inputs;
		var isUrls = typeof inputs[0] === 'string';

		if (isUrls) {
			Promise.all(inputs.map(fileUrl => {
				return new Promise((resolve, reject) => {
					fetch(fileUrl, {cache: 'no-store'}).then(async response => {
						const responseText = await response.text();
						if (!response.ok) {
							reject(new Error(`${fileUrl}: ${responseText}`));
							return;
						}
						const xml = (new DOMParser()).parseFromString(responseText, "application/xml");
						try {
							const docModels = await DocLoader.getDocumentsFromXml(xml, config);
							resolve(docModels);
						} catch (err) {
							reject(err);
						}
					}, err => {
						reject(err);
					});
				});
			})).then(docModels => {
				docModels = docModels.flat();
				DocLoader._loadCorpusFromDocModels(docModels, config);
			}, err => {
				config.callback.call(this, {error: err});
			});
		} else {
			Promise.all(Array.from(inputs).map(file => {
				return file.text().then(fileText => {
					return [file.name, fileText];
				});
			})).then(async fileDatas => {
				try {
					let docModels = [];
					const parser = new DOMParser();
					for (const fileData of fileDatas) {
						const xml = parser.parseFromString(fileData[1], 'application/xml');
						docModels = docModels.concat(await DocLoader.getDocumentsFromXml(xml, config));
					}
					DocLoader._loadCorpusFromDocModels(docModels, config);
				} catch (err) {
					config.callback.call(this, {error: err});
				}
			});
		}
	}

	static _loadCorpusFromDocModels(docModels, config) {
		if (docModels.length === 0) {
			config.callback.call(this, {error: new Error('No documents found for specified XPath: '+config.documents)});
			return;
		}

		if (config.indexDocument !== undefined && config.indexDocument !== '') {
			const numIndexDocs = docModels.filter(doc => doc.isDtocIndex).length;
			if (numIndexDocs === 0) {
				config.callback.call(this, {error: new Error('No index document found for specified XPath: '+config.indexDocument)});
				return;
			} else if (numIndexDocs > 1) {
				config.callback.call(this, {error: new Error('There can only be one index, but '+numIndexDocs+' were found.')});
				return;
			}
		}

		config.callback.call(this, {msg: 'Corpus created', corpus: new Corpus({
			documents: docModels,
			title: config.editionTitle,
			subTitle: config.editionSubtitle
		})});
	}

	static async getDocumentsFromXml(xml, config) {
		if (config.ignoreNamespace) {
			var root = xml.firstElementChild.nodeName;
			var rootPattern = new RegExp('<'+root+'.*?>'); // TODO also removes any other attributes
			var noRootNamespace = new XMLSerializer().serializeToString(xml).replace(rootPattern, '<'+root+'>');
			var parser = new DOMParser();
			xml = parser.parseFromString(noRootNamespace, 'application/xml');
		}

		let css = undefined;
		let xmlFirstChild = xml.firstChild;
		while (xmlFirstChild !== null && css === undefined) {
			if (xmlFirstChild.nodeName === 'xml-stylesheet') {
				const match = xmlFirstChild.nodeValue.match(/(href=)"(.*?)"/);
				if (match !== null) {
					css = match[2];
				}
			}
			xmlFirstChild = xmlFirstChild.nextSibling;
		}

		let xmlDocs = [xml];
		if (config.documents !== undefined && config.documents !== '') {
			xmlDocs = DocLoader.evaluateXPath(config.documents, xml, true);
		}

		const documents = [];
		for (const xmlDoc of xmlDocs) {
			const doc = await DocLoader.getDocumentFromXml(xmlDoc, config);
			if (doc !== undefined) {
				if (css !== undefined) doc.css = css;
				documents.push(doc);
			}	
		}
		
		if (config.indexDocument !== undefined && config.indexDocument !== '') {
			let indexDoc = DocLoader.evaluateXPath(config.indexDocument, xml, true);
			if (Array.isArray(indexDoc)) indexDoc = indexDoc[0];
			if (indexDoc !== undefined) {
				documents.push(new Document({
					xml: indexDoc,
					isDtocIndex: true
				}));
			}
		}

		return documents;
	}

	/**
	 * Takes an xml document/node and returns a DToC Document.
	 * @param {Node} xmlDoc 
	 * @param {Object} config
	 * @returns {Document}
	 */
	static async getDocumentFromXml(xmlDoc, config) {
		const currDoc = xmlDoc.cloneNode(true); // need to clone doc, otherwise xpaths search all docs??

		let docContent = currDoc;
		if (config.documentContent !== undefined && config.documentContent !== '') {
			config.documentContent = config.documentContent.replace(/^\/\//, '');
			docContent = DocLoader.evaluateXPath(config.documentContent, currDoc, true)[0];
		}
		if (docContent === undefined) {
			return undefined;
		}

		const docId = await DocLoader.generateIdForDoc(docContent);

		let docTitleStr = '';
		if (config.documentTitle !== undefined && config.documentTitle !== '') {
			config.documentTitle = config.documentTitle.replace(/^\/\//, '');
			let docTitle = DocLoader.evaluateXPath(config.documentTitle, docContent, true);
			docTitleStr = docTitle.length > 0 ? typeof docTitle[0] === 'string' ? docTitle[0].trim() : docTitle[0].textContent.trim() : '';
		}

		let docAuthorStr = '';
		if (config.documentAuthor !== undefined && config.documentAuthor !== '') {
			config.documentAuthor = config.documentAuthor.replace(/^\/\//, '');
			let docAuthor = DocLoader.evaluateXPath(config.documentAuthor, docContent, true);
			docAuthorStr = docAuthor.length > 0 ? typeof docAuthor[0] === 'string' ? docAuthor[0].trim() : docAuthor[0].textContent.trim() : '';
		}

		return new Document({
			id: docId,
			title: docTitleStr,
			author: docAuthorStr,
			xml: docContent,
			isDtocIndex: false
		});
	}

	/**
	 * Helper method for evaluating XPaths.
	 * @param {String} xpath The XPath to evaluate
	 * @param {Document|Node} docXML The XML document or node to use as context
	 * @param {Boolean} [xpath2] True to support XPath 2.0, false to not. Undefined means we check for the existence of XPath 2.0 functions.
	 * @returns {Array}
	 */
	static evaluateXPath(xpath, docXML, xpath2=undefined) {
		if (xpath2 === undefined) {
			for (const funcName of xpath2FunctionNames) {
				if (xpath.indexOf(funcName+'(') !== -1) {
					xpath2 = true;
					console.info('xpath 2:', xpath, ' uses ',funcName);
					break;
				}
			}
		}
		if (xpath2) {
			return DocLoader.evaluateXPathSaxon(xpath, docXML);
		} else {
			return DocLoader.evaluateXPathBrowser(xpath, docXML);
		}
	}

	/**
	 * Slow and thorough XPath 2.0 evaluation.
	 * @param {String} xpath The XPath to evaluate
	 * @param {Document|Node} docXML The XML document or node to use as context
	 * @returns {Array}
	 */
	static evaluateXPathSaxon(xpath, docXML) {
		try {
			return SaxonJS.XPath.evaluate(xpath, docXML, {resultForm: 'array'});
		} catch (e) {
			throw new Error('Error evaluating: '+xpath+'. '+e);
		}
	}

	/**
	 * Fast XPath 1.0 evaluation.
	 * @param {String} xpath The XPath to evaluate
	 * @param {Document|Node} docXML The XML document or node to use as context
	 * @returns {Array}
	 */
	static evaluateXPathBrowser(xpath, docXML) {
		var evalResult;
		if (docXML.nodeType === Node.DOCUMENT_NODE) {
			evalResult = docXML.evaluate(xpath, docXML, null, XPathResult.ANY_TYPE, null);
		} else {
			evalResult = docXML.ownerDocument.evaluate(xpath, docXML, null, XPathResult.ANY_TYPE, null);
		}

		var result;

		switch (evalResult.resultType) {
			case XPathResult.NUMBER_TYPE:
				result = evalResult.numberValue;
				break;
			case XPathResult.STRING_TYPE:
				result = evalResult.stringValue;
				break;
			case XPathResult.BOOLEAN_TYPE:
				result = evalResult.booleanValue;
				break;
			case XPathResult.UNORDERED_NODE_ITERATOR_TYPE:
			case XPathResult.ORDERED_NODE_ITERATOR_TYPE:
				result = [];
				var itResult = evalResult.iterateNext();
				while (itResult !== null) {
					result.push(itResult);
					itResult = evalResult.iterateNext();
				}
				break;
			case XPathResult.ANY_UNORDERED_NODE_TYPE:
			case XPathResult.FIRST_ORDERED_NODE_TYPE:
				result = evalResult.singleNodeValue;
				break;
		}

		if (Array.isArray(result) === false) {
			result = [result];
		}

		return result;
	}

	/**
	 * Uses SHA-256 to generate a hash of the document.
	 * @param {Node|String} doc A node or stringified version of the document
	 * @returns {String}
	 */
	static async generateIdForDoc(doc) {
		if (window.crypto === undefined || window.crypto.subtle === undefined) throw new Error('Browser is outdated!');

		let docString = doc;
		if (typeof docString !== 'string') {
			docString = new XMLSerializer().serializeToString(doc);
		}

		const encoder = new TextEncoder();
		const data = encoder.encode(docString);
		const hashAsArrayBuffer = await crypto.subtle.digest('SHA-256', data);
		const uint8ViewOfHash = new Uint8Array(hashAsArrayBuffer);
		const hashAsString = Array.from(uint8ViewOfHash)
			.map((b) => b.toString(16).padStart(2, '0'))
			.join('');
		return hashAsString;
	}

	/**
	 * Transforms a githubusercontent url to the equivalent jsdelivr url.
	 * Resources served through githubusercontent don't handle CORS properly.
	 * See https://github.com/orgs/community/discussions/24659
	 * @param {String} url 
	 * @returns {String}
	 */
	static convertGithubUrl(url) {
		const urlParts = url.replace('https://raw.githubusercontent.com/', '').split('/');
		if (urlParts.length > 3) {
			const user = urlParts[0];
			const repo = urlParts[1];
			const branch = urlParts[2];
			const file = urlParts.slice(3).join('/');
			url = 'https://cdn.jsdelivr.net/gh/'+user+'/'+repo+'@'+branch+'/'+file;
		} else {
			console.warn('Encountered githubusercontent but could not parse the URL:',url);
		}
		return url;
	}
}
