export default Ext.define('DToC.Panel', {
	constructor: function(config) {
		/*
		var toolsMap = {};
		var tools = [];
		
		// check to see if there are tool objects configured
		if (config.includeTools) {
			for (var tool in config.includeTools) {
				if (typeof config.includeTools[tool] == "object") {
					tools.push(config.includeTools[tool])
				}
			}
		}
		
		
		for (var tool in toolsMap) {
			if (config.includeTools && !config.includeTools[tool] || !toolsMap[tool]) {continue;}
			tools.push({
				type: tool,
				tooltip: this.localize(tool+"Tip"),
				callback: toolsMap[tool].fn,
				xtype: 'toolmenu',
				glyph: toolsMap[tool].glyph,
				items: toolsMap[tool].items
			})
		}

		Ext.apply(this, {
			tools: tools
		});
		*/
	},

	localize: function(key) {
		return key;
	},

	toastError: function(config) {
		if (Ext.isString(config)) {
			config = {html: config}
		}
		Ext.applyIf(config, {
			glyph: 'xf071@FontAwesome',
			title: this.localize("error")
		})
		this.toast(config);
	},
	
	toastInfo: function(config) {
		if (Ext.isString(config)) {
			config = {html: config}
		}
		Ext.applyIf(config, {
			glyph: 'xf05a@FontAwesome',
			title: this.localize("info")
		})
		this.toast(config);
	},
	
	toast: function(config) {
		if (Ext.isString(config)) {
			config = {html: config}
		}
		Ext.applyIf(config, {
			slideInDuration: 250,
			hideDuration: 250,
			autoCloseDelay: 1500,
			shadow: true,
			align: 'b',
			anchor: this.getTargetEl()
		})
		Ext.toast(config);
	}
});

Ext.define('DToC.ToolMenu', {
	extend: 'Ext.panel.Tool',
	alias: 'widget.toolmenu',
	renderTpl: ['<div class="x-menu-tool-hover">' + '</div>'+
			'<tpl if="glyph">' + 
			'<span id="{id}-toolEl" class="{baseCls}-glyph {childElCls}" role="presentation" style="font-family: {glyphFontFamily}; '+
				'<tpl if="Ext.os.name==\'iOS\'">'+ // FIXME: this is an awful hack..
					'margin-right: 15px; '+
				'</tpl>'+
			'">&#{glyph}</span>' + 
			'<tpl else>' + 
			'<img id="{id}-toolEl" src="{blank}" class="{baseCls}-img {baseCls}-{type}' + '{childElCls}" role="presentation"/>' + 
			'</tpl>'],
	privates: {
		onClick: function() {
			var me = this;
			var returnValue = me.callParent(arguments);

			if (returnValue) {
				me.showToolMenu.call(me);
			}

			return returnValue;
		},
		onDestroy: function() {
			Ext.destroyMembers(this, 'toolMenu'); //destructor
			this.callParent();
		}
	},
	showToolMenu: function() {
		if (this.items && this.items.length > 0) {
			if (!this.toolMenu || this.toolMenu.destroyed) {
				this.toolMenu = new Ext.menu.Menu({
					items: this.items
				});
			}
			this.toolMenu.showAt(0, 0);
			this.toolMenu.showAt(this.getX() + this.getWidth() - this.toolMenu.getWidth(), this.getY() + this.getHeight() + 10);
		}
	},
	initComponent: function() {
		var me = this;
		me.callParent(arguments);
	
		var glyph, glyphParts, glyphFontFamily;
		glyph = me.glyph || 'xf12e@FontAwesome';
	
		if (glyph) {
			if (typeof glyph === 'string') {
				glyphParts = glyph.split('@');
				glyph = glyphParts[0];
				glyphFontFamily = glyphParts[1];
			} else if (typeof glyph === 'object' && glyph.glyphConfig) {
				glyphParts = glyph.glyphConfig.split('@');
				glyph = glyphParts[0];
				glyphFontFamily = glyphParts[1];
			}
	
	
			Ext.applyIf(me.renderData, {
				baseCls: me.baseCls,
				blank: Ext.BLANK_IMAGE_URL,
				type: me.type,
				glyph: glyph,
				glyphFontFamily: glyphFontFamily
			});
		}
	}

});
