import Component from '../component.js';
import DocModelView from './view.js';

export default class DocModel extends Component {
	
	_model = {};
	get model() {
		return this._model;
	}
	set model(model) {
		this._model = model;
	}

	constructor(app) {
		super(app);
		this.view = new DocModelView({_controller: this});

		this.subscribe('loadedCorpus', function(src, corpus) {
			if (this.view.rendered) {
				this.view.buildProspect();
			}
			
			this.model = {};
			corpus.documents.forEach((doc) => {
				if (doc.isDtocIndex === false) {
					this.model[doc.id] = {
						index: {},
						tag: {},
						kwic: {}
					};
				}
			}, this);
		}, this);

		this.subscribe('documentLoaded', function(src, data) {
			const index = this.getCorpus().getDocumentIndex(data);
			this.view.setCurrentPosition(index, 0, false);
		}, this);
		
		this.subscribe('kwicSelected', function(src, data) {
			const index = this.getCorpus().getDocumentIndex(data);
			this.view.highlightProspect(index, data.tokenId, false);
		}, this);
		
		this.subscribe('tagSelected', function(src, data) {
			if (src != this && data.tokenId) {
				const index = this.getCorpus().getDocumentIndex(data);
				this.view.highlightProspect(index, data.tokenId, false);
			}
		}, this);
		
		this.subscribe('readerScroll', function(src, data) {
			if (!this.view.isArrowDragging) {
				this.view.chapterButtonContainer.hide();
				var amount;
				if (data.el.scrollTop == 0) {
					amount = 0;
				} else if (data.el.scrollHeight - data.el.scrollTop == data.el.clientHeight) {
					amount = 1;
				} else {
					amount = (data.el.scrollTop + data.el.clientHeight * 0.5) / data.el.scrollHeight;
				}
				const index = this.getCorpus().getDocumentIndex(data);
				this.view.setCurrentPosition(index, amount, false);
			}
		}, this);
		
		this.subscribe('tagsSelected', function(src, tags) {
			this.clearHits('tag');
			
			for (var i = 0; i < tags.length; i++) {
				var docTags = tags[i];
				for (var j = 0; j < docTags.length; j++) {
					var tag = docTags[j];
					this.view.showTokenHit(tag.docId, tag.tokenId, 'tag');
					try {
						this.model[tag.docId].tag[tag.tokenId] = true;
					} catch (err) {
						console.warn(err);
					}
				}
			}
		}, this);
		
		this.subscribe('indexesSelected', function(src, indexes) {
			this.clearHits('index');
			
			for (var i = 0; i < indexes.length; i++) {
				var index = indexes[i];
				this.view.showTokenHit(index.docId, index.tokenId, 'index');
				this.model[index.docId].index[index.tokenId] = true;
			}
		}, this);
		
		this.subscribe('kwicAdded', function(src, data) {
			this.clearHits('kwic');
			
			if (!Array.isArray(data)) {
				data = [data];
			}
			for (var i = 0; i < data.length; i++) {
				var d = data[i];
				this.view.showTokenHit(d.docId, d.tokenId, d.type);
				this.model[d.docId].kwic[d.tokenId] = true;
			}
		}, this);

		this.subscribe('clearSelections', function(src, data) {
			this.clearHits(data);
		}, this);
	}

	reset() {
		this.model = {};
	}

	clearHits(type) {
		for (let docId in this.model) {
			const doc = this.model[docId];
			if (type !== undefined) {
				doc[type] = {};
			} else {
				doc.index = {};
				doc.tag = {};
				doc.kwic = {};
			}
		};
		this.view.clearHits(type);
	}

	getSelectionsForDoc(docId) {
		return this.model[docId];
	}
}
