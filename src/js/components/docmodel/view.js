export default Ext.define('DToC.DocModel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.dtocDocModel',
	config: {
		indexDocIndex: undefined
	},

	MINIMUM_LIMIT: 1000,
	LINE_HEIGHT: 3,
	ARROW_HALF_HEIGHT: 6,
	CHAPTER_BUTTONS_DELAY: 250,

	segmentContainer: null,
	outlineTop: null,
	outlineLeft: null,
	outlineRight: null,
	outlineBottom: null,
	chapterButton: null,
	chapterButtonContainer: null,

	documents: new Ext.util.MixedCollection(),
	
	currentPosition: [0, 0], // [docIndex, amount]
	arrowDragSource: null,
	isArrowDragging: false,
	dragStartTime: null,

	constructor: function(config) {
		this._controller = config._controller;

		Ext.apply(config, {
			includeTools: {},
			id: 'dtcDocModel',
			title: '',
			width: 48,
			margin: '0 10 0 0',
			padding: '5 3'
		});
		
		this.callParent(arguments);

		Ext.DomHelper.append(document.body, '<div id="docModelCurrentSegment" class="black_arrow" style="display: none;"></div>'+
				'<div id="docModelOutlineTop"></div><div id="docModelOutlineLeft"></div><div id="docModelOutlineRight"></div><div id="docModelOutlineBottom"></div>'+
				'<div id="docModelChapterButton"></div>'
		);
	},

	initComponent: function() {
		var me = this;
		
		Ext.apply(me, {
			tools: null,
			cls: 'dtc-panel',
			height: '100%',
			layout: {
				type: 'vbox',
				align: 'center'
			},
			defaults: {
				layout: 'fit'
			},
			items: [{
				xtype: 'button',
				cls: 'dtc-button',
				padding: '0',
				text: 'Clear',
				handler: function() {
					this._controller.publish('clearSelections');
				},
				scope: this
			},{
				html: '<div id="docModelSegmentContainer" style="height: 100%; width: '+(me.width-10)+'px;"></div>',
				margin: '10 0 0 0',
				flex: 1
			}],
			listeners: {
				boxready: function() {
					this.setOutlineDimensions();
				},
				resize: function() {
					this.buildProspect();
					var corpusModel = this._controller.model;
					Object.keys(corpusModel).forEach(function(docId) {
						Object.keys(corpusModel[docId]).forEach(function(type) {
							Object.keys(corpusModel[docId][type]).forEach(function(tokenId) {
								this.showTokenHit(docId, tokenId, type);
							}, this);
						}, this);
					}, this);
				},
				afterlayout: function() {
					this.setOutlineDimensions();
					this.setCurrentPosition();
				},
				afterrender: function(panel) {
					this.initElements(panel);
				}
			}
		});
		
		me.callParent(arguments);
	},

	initElements: function(panel) {
		this.segmentContainer = Ext.get('docModelSegmentContainer');
		this.segmentContainer.unselectable();
		
		this.outlineTop = Ext.get('docModelOutlineTop');
		this.outlineLeft = Ext.get('docModelOutlineLeft');
		this.outlineRight = Ext.get('docModelOutlineRight');
		this.outlineBottom = Ext.get('docModelOutlineBottom');
		
		this.chapterButtonContainer = Ext.get('docModelChapterButton');
		this.chapterButton = new Ext.Button({
			text: 'Chapter',
			cls: 'dtc-button',
			renderTo: this.chapterButtonContainer,
			handler: function(b, e) {
				var docIndex = this.currentPosition[0];
				if (b.getText().indexOf('Next') != -1) {
					docIndex++;
				} else {
					docIndex--;
				}
				if (this.getIndexDocIndex() !== undefined && docIndex > this.getIndexDocIndex()) {
					docIndex++;
				}
				var docId = this._controller.getCorpus().getDocument(docIndex).id;
				this._controller.publish('documentSelected', {docId:docId});
				this.chapterButtonContainer.hide();
			},
			scope: this
		});
		
		Ext.get('docModelCurrentSegment').unselectable();
		
		this.arrowDragSource = new Ext.drag.Source({
			element: 'docModelCurrentSegment',
			constrain: {
				vertical: true
			},
			listeners: {
				dragmove: function(d, info, event) {
					var docIndex = this.currentPosition[0];
					var arrowY = info.element.current.y + this.ARROW_HALF_HEIGHT;
					var docDiv = this.segmentContainer.down('div:nth-child('+(docIndex+1)+')');
					var amount = (arrowY - docDiv.getY()) / docDiv.getHeight();
					var docId = this.documents.get(docIndex).document.id;
					
					this._controller.publish('docModelScroll', {
						docId: docId,
						amount: amount
					});
					
					var container = d.getConstrain().getRegion();
					if ((event.pageY >= container.bottom && docIndex < this._controller.getCorpus().getDocumentsCount()-1) ||
						(event.pageY <= container.top && docIndex > 0)) {
						if (this.dragStartTime == null) {
							this.dragStartTime = new Date();
						} else {
							var now = new Date();
							if (now.getTime() >= this.dragStartTime.getTime() + this.CHAPTER_BUTTONS_DELAY) {
								this.chapterButtonContainer.setY(arrowY - this.chapterButtonContainer.getHeight()/2);
								if (event.pageY >= container.bottom) {
									this.chapterButton.setText('Next Chapter');
									Ext.getCmp('dtcReader').setReaderScroll('bottom');
								} else {
									this.chapterButton.setText('Previous Chapter');
									Ext.getCmp('dtcReader').setReaderScroll('top');
								}
								this.chapterButtonContainer.show();
								// OLD TODO add dismiss timer for chapter button
							}
						}
					} else {
						this.chapterButtonContainer.hide();
					}
				},
				dragstart: function() {
					this.isArrowDragging = true;
					this.dragStartTime = null;
					this.chapterButtonContainer.hide();
				},
				dragend: function() {
					this.isArrowDragging = false;
					this.dragStartTime = null;
				},
				scope: this
			}
		});
		
		panel.body.addListener('click', function(e) {
			var target = e.getTarget(null,null,true);
			if (target && target.dom.tagName=='IMG') {
				var parts = target.dom.id.split('_');
				var docIndex = parseInt(parts[1]);
				var docLine = parseInt(parts[2]);
				
				var docId = this.documents.get(docIndex).document.id;
				
				var tokenId = target.getAttribute('tokenId');
				if (tokenId) {
					var type = target.dom.className.replace('docModelLine', '').trim();
//						console.log(type);
					this._controller.publish('tagSelected', {
						tokenId: tokenId,
						docId: docId,
						type: type
					});
				} else {
					var clickY = e.getY();
					var docDiv = target.parent('div');
					var docY = docDiv.getY();
					var amount = (clickY - docY) / docDiv.getHeight();
					
					this._controller.publish('docModelClick', {
						docId: docId,
						amount: amount
					});
				}
			}
			this.chapterButtonContainer.hide();
		}, this);
	},

	buildProspect: function() {
		if (this._controller.getCorpus()) {
			this.setIndexDocIndex(undefined);
			var indexInDocsMod = 0;
			var totalTokens = 0;
			this._controller.getCorpus().documents.forEach((doc, index) => {
				// check to see if this doc was specified as the index (via the cwrc interface)
				if (doc.isDtocIndex === true) {
					indexInDocsMod = 1;
					this.setIndexDocIndex(index);
				} else {
					totalTokens += doc.termsCount;
				}
			}, this);
			
			var docsCount = this._controller.getCorpus().getDocumentsCount();
			var containerHeight = this.segmentContainer.getHeight();
			var separationHeight = (docsCount-1-indexInDocsMod) * this.LINE_HEIGHT;
			containerHeight -= separationHeight;
			var availableLines = parseInt(containerHeight / this.LINE_HEIGHT);
			if (this.LINE_HEIGHT * availableLines > containerHeight) {
				availableLines--; // make sure there's no scrollbar for prospect
			}
			
			var tokensPerLine = Math.floor(totalTokens / availableLines);
			if (tokensPerLine < this.MINIMUM_LIMIT) {tokensPerLine = this.MINIMUM_LIMIT;}
			// this.setApiParams({limit: tokensPerLine}); // TODO
			
			var docTotalTokens, linesPerDocument;
			var imagesSnippet = "";
			this.documents = new Ext.util.MixedCollection();
			
			this._controller.getCorpus().documents.forEach((doc, index) => {
				if (doc.isDtocIndex !== true) {
					var tiplabel = doc.getShortTitle();
					imagesSnippet += "<div>";
					docTotalTokens = doc.termsCount;
					
					var percentageOfWhole = docTotalTokens / totalTokens;
					//linesPerDocument = Math.floor(docTotalTokens / tokensPerLine);
					linesPerDocument = Math.floor(availableLines * percentageOfWhole);
		//			console.log('linesPerDocument',linesPerDocument,'percentageOfWhole',percentageOfWhole);
					if (linesPerDocument < 1) {linesPerDocument = 1;}
					
					// OLD TODO change ID system to reflect new token IDs
					for (var j = 0; j < linesPerDocument; j++) {
						imagesSnippet += "<img src='"+Ext.BLANK_IMAGE_URL+"' class='docModelLine' "+
		//						"'ext:qtip='"+tiplabel+"' "+
								"id='prospect_"+index+'_'+j+"' />";
					}
					this.documents.add(index, {
						document: doc, lines: linesPerDocument
					});
					imagesSnippet += '</div>';
				}
			}, this);
			this.segmentContainer.setHtml(imagesSnippet);
		}
	},

	setCurrentPosition: function(docIndex, amount, animate) {
		docIndex = docIndex == null ? this.currentPosition[0] : docIndex;
		amount = amount == null ? this.currentPosition[1] : amount;
		animate = animate == null ? false : animate;
		
		if (this.getIndexDocIndex() !== undefined && docIndex > this.getIndexDocIndex()) {
			docIndex--;
		}

		var docContainer = this.segmentContainer.down('div:nth-child('+(docIndex+1)+')');
		if (docContainer) {
			var height = docContainer.getHeight();
			var y = docContainer.getY() + Math.round(height * amount) - this.ARROW_HALF_HEIGHT;
			
			var currSeg = Ext.get('docModelCurrentSegment');
			if (!currSeg.isVisible()) currSeg.show();
			if (animate) {
				currSeg.setY(y, animate);
			} else {
				currSeg.setStyle({top: y+'px'});
			}
			
			var box = docContainer.getBox();
			this.arrowDragSource.setConstrain({
				vertical: true,
				region: new Ext.util.Region(box.y-this.ARROW_HALF_HEIGHT, box.x, box.y+box.height+this.ARROW_HALF_HEIGHT, box.x)
			});
			
			this.currentPosition = [docIndex, amount];
		}
	},

	highlightProspect: function(docIndex, tokenId, animate) {
		var doc = this.documents.get(docIndex);
		var tokenPercent = this.getTokenPercent(doc.document, tokenId);
		this.setCurrentPosition(docIndex, tokenPercent, animate);
	},

	setOutlineDimensions: function(minMaxObj) {
		var outlineThickness = 1;
		var min = 0;
		var max = 1;
		if (minMaxObj) {
			min = minMaxObj.min;
			max = minMaxObj.max;
		}
		
		var box = this.segmentContainer.getBox();
		
		// adjust for padding
		box.x -= 5;
		box.width += 10;
		box.y -= 5;
		box.height += 10;
		
		// apply token ranges
		box.y += min * box.height;
		box.height = max * box.height - (min * box.height);
		
		if (box.height < 18) {
			var outlineHeight = Math.round(box.height*0.5);
			
			this.outlineTop.setBox({x: box.x, y: box.y, width: box.width, height: outlineHeight});
			this.outlineLeft.setBox({x: box.x, y: box.y+outlineHeight, width: 0, height: 0});
			this.outlineRight.setBox({x: box.x+box.width-outlineThickness, y: box.y+outlineHeight, width: 0, height: 0});
			this.outlineBottom.setBox({x: box.x, y: box.y+outlineHeight, width: box.width, height: outlineHeight});
			
			this.outlineTop.setStyle('border-radius', outlineHeight+'px '+outlineHeight+'px 0 0');
			this.outlineBottom.setStyle('border-radius', '0 0 '+outlineHeight+'px '+outlineHeight+'px');
		} else {
			this.outlineTop.setBox({x: box.x, y: box.y, width: box.width, height: 9});
			this.outlineLeft.setBox({x: box.x, y: box.y+9, width: outlineThickness, height: box.height-18});
			this.outlineRight.setBox({x: box.x+box.width-outlineThickness, y: box.y+9, width: outlineThickness, height: box.height-18});
			this.outlineBottom.setBox({x: box.x, y: box.y+box.height-12, width: box.width, height: 9});
			
			this.outlineTop.setStyle('border-radius', null);
			this.outlineBottom.setStyle('border-radius', null);
		}
		
		var currSeg = Ext.get('docModelCurrentSegment');
		var viz = currSeg.isVisible();
		if (!viz) currSeg.show();
		currSeg.setX(box.x+box.width-5);
		if (!viz) currSeg.hide();
		
		this.chapterButtonContainer.hide();
		this.chapterButtonContainer.setX(box.x+box.width+7);
	},

	showTokenHit: function(docId, tokenId, type) {
		var doc = this._controller.getCorpus().getDocument(docId);
		var index = this._controller.getCorpus().getDocumentIndex(doc);
		if (tokenId) {
			var tokenPercent = this.getTokenPercent(doc, tokenId);
			var i = Math.floor(this.documents.get(index).lines * tokenPercent);
			try {
				var el = Ext.get('prospect_' + index + '_' + i);
				if (el !== null) {
					el.dom.className = 'docModelLine '+type;
					el.set({tokenId: tokenId});
				}
			} catch (e) {
			}
		} else {
			console.warn('no token id');
		}
	},
	
	getTokenPercent: function(doc, tokenId) {
		var tokenPosition = parseInt(tokenId.split('_')[1]);
		var tokenPercent;
		// TODO calculate tag offsets
		// if (tokenId.match('tag') !== null) {
		// 	tokenPercent = tokenPosition / doc.get('lastTokenStartOffset-lexical');
		// } else {
			tokenPercent = tokenPosition / doc.tokensCount;
		// }
		return tokenPercent;
	},
	
	clearHits: function(type) {
		var selector = 'img';
		if (type != null) {
			selector = 'img[class*='+type+']';
		}
		var imgs = Ext.DomQuery.select(selector, this.segmentContainer.dom);
		for (var i = 0; i < imgs.length; i++) {
			var hit = Ext.get(imgs[i]);
			hit.dom.className = 'docModelLine';
			hit.dom.removeAttribute('tokenId');
		}
	}
});
