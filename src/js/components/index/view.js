import DToCPanel from '../../app/panel.js';

export default Ext.define('IndexView', {
	extend: 'Ext.tree.Panel',
	mixins: ['DToC.Panel'],

	treeEditor: null,
	currentChapterFilter: null,
	ignoreNextChapterFilter: false,

	_maskEl: null,

	constructor: function(config) {
		this._controller = config._controller;

		var treeStore = Ext.create('Ext.data.TreeStore', {
			batchUpdateMode: 'complete',
			autoLoad: false,
			root: {
				expanded: true,
				draggable: false
			}
		});
		
		var treeConfig = {
			title: 'Index',
			id: 'dtcIndex',
			bodyCls: 'dtc-tree',
			useArrows: true,
			lines: false,
			rootVisible: false,
			scrollable: 'y',
			hideHeaders: true,
			viewConfig: {
				scrollable: 'y'
			},
			columns: {
				items: [{
					xtype: 'treecolumn',
					width: '100%',
					renderer: function(value, metadata) {
						var r = metadata.record;
						if (r.getData().targetMatches !== true && r.getData().isCrossRef !== true) {
							metadata.tdCls += ' disabled';
						}
						if (r.getData().isCrossRef === true) {
							metadata.tdCls += ' crossRef';
						}
						value = r.data.text;
						return value;
					}
				}]
			},
			selModel: new Ext.selection.TreeModel({
				mode: 'MULTI',
				listeners: {
					beforeselect: function(sel, record) {
						// cancel if there are no target matches
						return record.getData().targetMatches === true || record.getData().isCrossRef === true;
					},
					selectionchange: function(sm, nodes) {
						this._controller._app.view.showMultiSelectMsg(this);
						
						var indexes = [];
						var crossRef = null;
						for (var i = 0; i < nodes.length; i++) {
							var node = nodes[i];
							if (!node.isLeaf()) {
								node.expand();
							}
							if (node.getData().isCrossRef) {
								crossRef = node.getOwnerTree().getRootNode().findChild('indexId', node.getData().targets[0]);
								if (crossRef === null) {
									console.log('no cross ref found for:', node.getData().targets[0]);
								}
								break;
							} else {
								var t = node.getData().targets;
								for (var j = 0; j < t.length; j++) {
									var id = t[j];
									var data = {};
									var indexObj = this._controller.indexTargetData[id];
									if (Ext.isObject(indexObj)) {
										Ext.apply(data, indexObj);
										data.label = (node.parentNode.getData().textNoCount ? node.parentNode.getData().textNoCount : '') + ' ' + node.getData().textNoCount;
										indexes.push(data);
									} else {
										if (window.console) {
											console.log('no targets:',node.data.textNoCount+', id:'+id);
										}
									}
								}
							}
						}
						if (crossRef !== null) {
							node.getOwnerTree().selectPath(crossRef.getPath());
						} else {
							this._controller.publish('indexesSelected', indexes);
						}
					},
					scope: this
				}
			}),
			store: treeStore,
			tbar: new Ext.Toolbar({
				itemId: 'topToolbar',
				cls: 'dtc-toolbar',
				hideBorders: true,
				items: [{
					xtype: 'textfield',
					itemId: 'filter',
					emptyText: 'Filter',
					width: 120,
					enableKeyEvents: true,
					
					keyupDelay: 150,
					lastKeyupTimestamp: undefined,
					keyupTimeoutId: undefined,
					
					listeners: {
						keyup: function(field, event) {
							clearTimeout(field.initialConfig.keyupTimeoutId);
							var currTime = Date.now();
							if (currTime - field.initialConfig.lastKeyupTimestamp > field.initialConfig.keyupDelay) {
								var value = field.getValue();
								if (value == '') {
									this.filterIndex();
									this.collapseAll();
									field.initialConfig.lastKeyupTimestamp = undefined;
									return;
								} else {
									var regex = new RegExp('.*'+value+'.*', 'i');
									var nodesToExpand = [];
									this.filterIndex();
									this.getStore().filterBy(function(r) {
										var match = false;
										if (r.hasChildNodes()) {
											r.eachChild(function(n) {
												if (n.get('text').match(regex) !== null) {
													match = true;
													nodesToExpand.push(r);
													return false;
												}
											}, this);
										} else {
											match = r.get('text').match(regex) !== null;
										}
										return match;
									});
									for (var i = 0; i < nodesToExpand.length; i++) {
										nodesToExpand[i].expand();
									}
								}
							} else {
								field.initialConfig.keyupTimeoutId = setTimeout(function(){field.fireEvent('keyup', field);}.bind(this), field.initialConfig.keyupDelay);
							}
							field.initialConfig.lastKeyupTimestamp = currTime;
						},
						scope: this
					}
				}, '->', {
					text: 'Filter by Chapter',
					itemId: 'indexChapterFilter',
					menu: {
						items: [],
						plain: true,
						showSeparator: false,
						listeners: {
							click: function(menu, item) {
								this.doChapterFilter(item.initialConfig.docId, true);
							},
							scope: this
						}
					}
				}]
			})
		};
		
		Ext.apply(config, treeConfig);
		
		this.callParent(arguments);
		this.mixins['DToC.Panel'].constructor.apply(this, arguments);


	},

	reset: function() {
		var topdock = this.getDockedComponent('topToolbar');
		if (topdock.down('#issuesButton')) topdock.down('#issuesButton').destroy();
		this.getStore().getRootNode().removeAll();
		this.getStore().clearFilter();
		this.currentChapterFilter = null;
	},

	remove: function() {
		this.up().remove(this, true);
	},

	clearSelections: function() {
		this.getSelectionModel().deselectAll(true);
	},

	// reset index filter and then filter out all records with no matches
	filterIndex: function() {
		this.getStore().clearFilter();
		this.getStore().filterBy(function(r) {
			// TODO need to handle cross refs that don't point to a target
			return r.getData().targetMatches === true || r.getData().isCrossRef === true;
		}, this);
	},

	updateIndexProgress: function(progress) {
		var msgEl = this._maskEl.down('.x-mask-msg-text');
		if (msgEl) {
			msgEl.dom.firstChild.data = 'Processing Index: '+Math.floor(progress*100)+'%';
		}
	},

	updateChapterFilter: function() {
		var menu = this.down('#indexChapterFilter').getMenu();
		menu.removeAll();
		
		this._controller.getCorpus().documents.forEach((doc) => {
			if (doc.isDtocIndex !== true) {
				menu.add({
					xtype: 'menucheckitem',
					docId: doc.id,
					group: 'indexchapters',
					text: doc.getShortTitle()
				});
			}
		}, this);
	},
	
	doChapterFilter: function(docId, local) {
		if (this.ignoreNextChapterFilter) {
			this.ignoreNextChapterFilter = false;
			return;
		}
		
		if (docId === this.currentChapterFilter) {
			docId = null;
		}
		
		var menuItem;
		this.down('#indexChapterFilter').getMenu().items.each(function(item) {
			if (item.initialConfig.docId === docId) {
				menuItem = item;
			} else {
				item.setChecked(false);
			}
		}, this);
		
		if (docId === null) {
			this.currentChapterFilter = null;
			this.collapseAll();
			this.filterIndex();
			docId = null;
		} else {
			menuItem.setChecked(true);
			this.currentChapterFilter = docId;
			this.filterIndex();
			this.getStore().filterBy(function(record, id) {
				var t = record.getData().targets || [];
				for (var i = 0; i < t.length; i++) {
					var id = t[i];
					var indexObj = this._controller.indexTargetData[id];
					if (indexObj && indexObj.docId === docId) {
						return true;
					}
					return false;
				}
			}, this);
		}
		if (local) {
			this.ignoreNextChapterFilter = true;
			this._controller.publish('chapterFilterSelected', docId);
		}
	},
	
	editNodeLabel: function(node) {
		if (node.getData().editable != false) {
			this.treeEditor.startEdit(node.ui.textNode);
		}
	}
})
