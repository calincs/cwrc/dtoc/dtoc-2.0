import Component from "../component";
import ToCView from "./view.js";

export default class ToC extends Component {

	_docId = undefined;
	get docId() {
		return this._docId;
	}
	set docId(docId) {
		this._docId = docId;
	}

	_titlesMode = null;
	get titlesMode() {
		return this._titlesMode;
	}
	set titlesMode(mode) {
		if (mode == this.MIN_TITLES) {
			this.titlesMode = this.MIN_TITLES;
		} else {
			this.titlesMode = this.FULL_TITLES;
		}
		// this.setChapterTitles(true);
	}
	FULL_TITLES = 0;
	MIN_TITLES = 1;

	constructor(app, config) {
		super(app);

		this.view = new ToCView({_controller: this, isCurator: false});

		this.subscribe('loadedCorpus', function(src, corpus) {
			let title = 'Table of Contents';
			if (!config || config.showTitle !== false) {
				if (corpus.title !== '') {
					title = corpus.title;
				}
				if (corpus.subTitle !== '') {
					title += '<br>'+corpus.subTitle;
				}
			}
			this.view.setTitle(title)

			this.view.initToc(this.getDocNodes(), true);
		}, this);

		this.subscribe('documentLoaded', function(src, data) {
			this.docId = data.docId;
			var match = this.view.getStore().query('docId', this.docId).first();
			if (match) {
				var currSel = this.view.getSelection();
				if (currSel == null || currSel.length === 0 || currSel[0].parentNode !== match) {
					this.view.internalSelection = true;
					this.view.setSelection(match);
					this.view.internalSelection = false;
				}
			}
		}, this);

		this.subscribe('tagsSelected', function(src, tags) {
			var processedTags = [];
			var identifiers = {};
						
			for (var i = 0; i < tags.length; i++) {
				var docTags = tags[i];
				for (var j = 0; j < docTags.length; j++) {
					var tagData = docTags[j];
					
					tagData.identifier = this.getIdentifier(tagData, 'TAG');
					identifiers[tagData.identifier] = true;
					
					var index = this.view.getStore().findBy(function(r) {
						return r.get('identifier') === tagData.identifier;
					});
					
					if (index === -1) {
						processedTags.push(tagData);
					} else {
						// console.log('exists', tagData.identifier);
					}
				}
			}
			
			this.view.removeNodes('tag', identifiers);
			
			this.view.addTagsToTree(processedTags);
		}, this);

		this.subscribe('indexesSelected', function(src, indexes) {
			this.view.removeNodes('index');
			this.view.addIndexesToTree(indexes);
		}, this);

		this.subscribe('clearSelections', function(src, data) {
			this.view.clearTree();
		}, this);
	}

	reset() {
		this.view.getStore().getRootNode().removeAll();
	}

	getDocNodes() {
		return this.getCorpus().documents
			.filter(doc => !doc.isDtocIndex)
			.map(doc => {
				var title, author;
				if (this.titlesMode === this.MIN_TITLES) {
					title = doc.getShortTitle().normalize();
					title = title.slice(0, title.length-1);
				} else {
					title = doc.title.normalize();
				}
				if (!this.isCurator) {
					author = doc.author;
					if (author !== undefined && author !== '') {
						title += '<br/><span class="author">'+author+'</span>';
					}
				}
				return {
					docId: doc.id,
					text: title
				}
			}, this);
	}

	getKwics(query) {
		this.view.removeNodes('kwic');
		const me = this;
		this._app.search(query).then(results => {
			if (results.length === 0) {
				me.view.toastInfo({
					html: 'No results found for: '+query,
					align: 't'
				});
				return;
			}

			let contexts = [];
			
			if (this._app.isDebug) console.time('getContext');
			results.forEach(docResult => {
				docResult.matches.forEach(match => {
					const context = me.getContextForTerm(docResult.docId, match.index, 6);
					contexts.push({
						docId: docResult.docId,
						middle: match.term,
						left: context.left,
						right: context.right,
						position: match.index
					});
				})
			});
			if (this._app.isDebug) console.timeEnd('getContext');

			me.view.addKwicsToTree(contexts);
		});
	}

	async getAutoCompletes(query) {
		if (query.endsWith('*') === false) query += '*';

		const termCounts = {};
		const results = await this._app.search(query);
		results.forEach(docResult => {
			docResult.matches.forEach(match => {
				const term = match.term.toLowerCase();
				if (termCounts[term] === undefined) {
					termCounts[term] = 0;
				}
				termCounts[term]++;
			})
		});

		let totalCount = 0;
		let suggestions = [];
		for (const term in termCounts) {
			totalCount += termCounts[term];
			suggestions.push([term, termCounts[term]]);
		}
		suggestions.sort((a,b) => {
			return b[1] - a[1];
		});
		suggestions.unshift([query, totalCount]);

		return suggestions;
	}

	getContextForTerm(docId, position, contextLength) {
		var context = {left: '', right: ''};
		
		function getSiblings(node, direction) {
			var siblings = [];
			
			var walker = node.ownerDocument.createTreeWalker(node.ownerDocument, NodeFilter.SHOW_TEXT);
			walker.currentNode = node.firstChild;
			var count = 0;
			if (direction === 'prev') {
				var prevNode = walker.previousNode();
				while (prevNode !== null && count < contextLength) {
					siblings.unshift(prevNode.data);
					if (prevNode.parentNode.nodeName === 'span') {
						count++;
					}
					prevNode = walker.previousNode();
				}
			} else {
				var nextNode = walker.nextNode();
				while (nextNode !== null && count < contextLength) {
					siblings.push(nextNode.data);
					if (nextNode.parentNode.nodeName === 'span') {
						count++;
					}
					nextNode = walker.nextNode();
				}
			}
			
			return siblings;
		}

		var docXml = this.getCorpus().getDocument(docId).xml;
		var termNode = docXml.querySelector('span[tokenId="word_'+position+'"]');
		if (termNode === null) {
			console.warn('could not locate term node',docId,position);
		} else {
			context.left = getSiblings(termNode, 'prev').join('');
			context.right = getSiblings(termNode, 'next').join('');
		}

		return context;
	}

	getIdentifier(data, type) {
		let id = data.docId+'-'+data.tokenId;
		if (type === 'TAG') {
			id += '-'+data.xpath;
		}
		return id;
	}

	getExportData() {
		return this.view.getDocIdsOrdering();
	}

	showEditView() {
		this._doViewSwitch(true);
	}

	showDefaultView() {
		this._doViewSwitch(false);
	}

	_doViewSwitch(isCurator) {
		const container = this.view.up();
		container.remove(this.view);
		
		this.view = new ToCView({_controller: this, isCurator: isCurator, initData: this.getDocNodes()});

		container.insert(2, this.view);
	}
}
