import DToCPanel from '../../app/panel.js';

export default Ext.define('ToCView', {
	extend: 'Ext.tree.Panel',
	mixins: ['DToC.Panel'],
	alias: 'widget.dtocToc',
	
	initialized: false,
	xpathWin: null,
	internalSelection: false,

	isCurator: false,
	
	toolTipConfig: {
		cls: 'dtcReaderNote',
		showDelay: 250,
		hideDelay: 0,
		constrainPosition: true,
		border: false,
		shadow: false,
		padding: 5,
		maxWidth: 400
	},
	
	constructor: function(config) {
        this._controller = config._controller;
		this.isCurator = config.isCurator;

        Ext.applyIf(config, {
			title: 'Table of Contents',
			id: 'dtcToc',
			width: 275,
			minWidth: 175,
			animCollapse: false,
			collapseDirection: 'left',
			listeners: {
				collapse: function(p) {
					p.el.down('.x-panel-header').addCls('borderRadiusTop borderRadiusBottom');
				}
			}
        });
		
		var treeStore = Ext.create('Ext.data.TreeStore', {
			fields: ['identifier', 'docId', 'tokenId', 'sortId', 'dtcType'],
			batchUpdateMode: 'complete',
			autoLoad: false,
			root: {
				expanded: true,
				draggable: false
			}
		});
		
		var treeConfig = {
			itemId: 'dtcTree',
			xtype: 'treepanel',
			cls: 'dtc-panel',
			bodyCls: 'dtc-tree',
			useArrows: true,
			lines: false,
			rootVisible: false,
			hideHeaders: true,
			scrollable: 'y',
			viewConfig: {
				scrollable: 'y',
				listeners: {
					select: function(view, record, index, opts) {
						if (!this.internalSelection) {
							var data = record.getData();
							// document
							if (data.isDoc) {
								this.showDocument(data.docId);
							} else {
							
								switch(data.dtcType) {
								case 'tag':
									var tag = data.tagData;
									var edata = {
										xpath: tag.xpath,
										tokenId: tag.tokenId,
										docId: data.docId,
										type: 'tag'
									};
									this._controller.publish('tagSelected', edata);
									break;
								case 'index':
									var index = data.indexData;
									var edata = {
										tag: index.tag,
										tokenId: index.tokenId,
										docId: data.docId,
										type: 'index'
									};
									this._controller.publish('tagSelected', edata);
									break;
								case 'kwic':
									var data = {
										tokenId: data.tokenId,
										docId: data.docId,
										type: 'kwic'
									};
									this._controller.publish('kwicSelected', data);
								}
							}
						}
					},
					scope: this
				}
			},
			store: treeStore,
			tools: null,
			tbar: {
				cls: 'dtc-toolbar',
				hideBorders: true,
				items: [{
					xtype: 'combo',
					itemId: 'search',
					width: '100%',
					enableKeyEvents: true,
					minChars: 2,
					autoSelect: false,
					multiSelect: false,
					hideTrigger: true,
					valueField: 'term',
					displayField: 'term',
					tpl: [
						'<ul class="x-list-plain"><tpl for=".">',
						'<li role="option" class="x-boundlist-item" style="white-space: nowrap;">{term} ({count})</li>',
						'</tpl></ul>'
					],
					store: Ext.create('Ext.data.ArrayStore', {
						fields: ['term', 'count'],
						listeners: {
							beforeload: function(store, operation) {
								var query = operation.getParams()['query'];
								if (query.trim() !== '') {
									this._controller.getAutoCompletes(query).then(results => {
										store.loadData(results);
									})
								}
							},
							scope: this
						}
					}),
					listeners: {
						select: function(combo, record) {
							var query = record.get('term');
							this._controller.getKwics(query);
						},
						keypress: function(cmp, evt) {
							if (evt.getKey() === 13) {
								var query = cmp.getRawValue();
								this._controller.getKwics(query);
							}
						},
						scope: this
					}
				}]
			},
			listeners: {}
		};
		if (config.initData !== undefined) {
			treeConfig.listeners.afterrender = {
				fn: function() {
					this.initToc(config.initData);
				},
				scope: this,
				single: true
			};
		}
		
		if (this.isCurator) {
			treeConfig.viewConfig.plugins = {
				ptype: 'treeviewdragdrop',
				enableDrag: true,
				enableDrop: true,
				containerScroll: true
			};
			
			treeConfig.viewConfig.listeners.beforedrop = function(targetNode, dragData, overModel, dropPosition, dropHandlers, event) {
				if ((dragData.records[0].getData().isDoc && dropPosition === 'append') || !overModel.getData().isDoc) return false;
			};
			treeConfig.viewConfig.listeners.drop = {
				fn: function(node, data, overModel, dropPosition) {
					var docIds = this.getDocIdsOrdering();
					// TODO move this?
					this._controller.getCorpus().setDocumentsOrder(docIds);
					Ext.getCmp('dtcDocModel').buildProspect();
				},
				scope: this
			};
			
			treeConfig.bbar = {
			   height: 30,
			   style: {borderTopWidth: '1px !important'},
			   items: [{
				   xtype: 'container',
				   html: '<span>Drag and drop to re-order chapters.</span>'
			   }]
			};
		}
		
		Ext.apply(config, treeConfig);
		
		this.callParent(arguments);
		this.mixins['DToC.Panel'].constructor.apply(this, arguments);

	},
	initComponent: function() {
		var me = this;
		
		me.callParent(arguments);
	},
	
	initToc: function(data, forceInit) {
		if (!this.initialized || forceInit === true) {
			if (data) {
				var root = this.getRootNode();
				root.removeAll();
				for (var i = 0; i < data.length; i++) {
					var d = data[i];
					var doc = this._controller.getCorpus().getDocument(d.docId);
					var node = root.appendChild(this.createChapterTreeNode(d.text, doc));
					this._createToolTipForChapterNode(node);
				}
			}
			this.initialized = true;
		}
	},
	
	// setChapterTitles: function(modifyCurrent) {
    //     console.log('setChapterTitles', modifyCurrent)
	// 	var root = this.getRootNode();
		
	// 	this._controller.getCorpus().documents.forEach((doc) => {
	// 		if (doc.isDtocIndex !== true) {
	// 			var docNode, title, author;
	// 			if (this.titlesMode == this.MIN_TITLES) {
	// 				title = doc.getShortTitle().normalize();
	// 				title = title.slice(0, title.length-1);
	// 			} else {
	// 				title = doc.title.normalize();
	// 			}
	// 			if (!this.isCurator) {
	// 				author = doc.author;
	// 				if (author !== undefined && author !== '') {
	// 					title += '<br/><span class="author">'+author+'</span>';
	// 				}
	// 			}
	// 			if (modifyCurrent) {
	// 				docNode = root.findChild('docId', doc.id);
	// 				if (docNode) {
	// 					docNode.set('text', title);
	// 				}
	// 			} else {
	// 				docNode = root.appendChild(this.createChapterTreeNode(title, doc));
	// 			}
				
	// 			if (docNode.getData().toolTip == null) {
	// 				this._createToolTipForChapterNode(docNode);
	// 			} else {
	// 				if (this.titlesMode == this.MIN_TITLES) {
	// 					docNode.getData().toolTip.enable();
	// 				} else {
	// 					docNode.getData().toolTip.disable();
	// 				}
	// 			}
	// 		}
	// 	}, this);
	// },
	
	createChapterTreeNode: function(title, doc) {
		var data = {
			text: title,
			expandable: true,
			editable: true,
			leaf: true,
			docId: doc.id,
			isDoc: true,
			listeners: {
				beforeexpand: function(node) {
					if (!node.hasChildNodes()) {
						return false;
					}
				},
				scope: this
			}
		};
		return data;
	},
	
	_createToolTipForChapterNode: function(treeNode) {
		if (treeNode.getData().docId != null) {
			var root = this.getRootNode();
			var doc = this._controller.getCorpus().getDocument(treeNode.getData().docId);
			var docNode = root.findChild('docId', doc.id);
			if (docNode) {
				var nodeEl = this.getView().getNode(docNode);
				// OLD TODO destroy tooltip when treenode is removed
				var tip = Ext.create('Ext.ToolTip', Ext.apply(this.toolTipConfig, {
					target: nodeEl,
					title: '',
					listeners: {
						show: function(tt) {
							tt.update('<span class="title">'+doc.title+'</span>'+
									'<br/><span class="author">'+doc.author+'</span>');
						},
						scope: this
					}
				}));
				docNode.set('toolTip', tip);
				
				if (this._controller.titlesMode == this._controller.MIN_TITLES) {
					docNode.getData().toolTip.enable();
				} else {
					docNode.getData().toolTip.disable();
				}
			}
		}
	},

	// getTitlesMode: function() {
	// 	return this.titlesMode;
	// },
	
	// setTitlesMode: function(mode) {
	// 	if (mode == this.MIN_TITLES) {
	// 		this.titlesMode = this.MIN_TITLES;
	// 	} else {
	// 		this.titlesMode = this.FULL_TITLES;
	// 	}
	// 	this.setChapterTitles(true);
	// },
	
	showDocument: function(docId) {
		this._controller.publish('documentSelected', {docId:docId});
	},
	
	addKwicsToTree: function(kwics) {
		if (kwics.length > 0) {
			var childToSelect = undefined;
			
			var docsToExpand = [];
			
			var root = this.getRootNode();
			var kwic, doc, docId, text, tokenId;
			var kwicsForEvent = [];
			for (var i = 0; i < kwics.length; i++) {
				kwic = kwics[i];
				docId = kwic.docId;
				doc = root.findChild('docId', docId);
				if (doc !== null) {
					text = kwic.left + ' <span class="dtc-kwic-highlight">' + kwic.middle + '</span> ' + kwic.right;
					
					tokenId = 'word_'+kwic.position;
					
					kwicsForEvent.push({docId: docId, tokenId: tokenId, type: 'kwic'});
					
					doc.collapse();
					if (docsToExpand.indexOf(doc) === -1) {
						docsToExpand.push(doc);
					}
					
					var child = doc.appendChild({
						text: text,
						leaf: true,
						cls: 'kwic child',
						editable: false,
						draggable: false,
						tokenId: tokenId,
						sortId: kwic.position,
						docId: docId,
						dtcType: 'kwic'
					});
					
					if (docId === this._controller.docId && childToSelect === undefined) {
						childToSelect = child.id;
					}
				} else {
					throw new Error('ToC.addKwicsToTree: no doc found for '+docId);
				}
			}
			
			for (var i = 0; i < docsToExpand.length; i++) {
				docsToExpand[i].expand();
			}

			this.sortTree();

			this._controller.publish('kwicAdded', kwicsForEvent);

			if (childToSelect !== undefined) {
				this.setSelection(this.getStore().findRecord('id', childToSelect));
			}
		}
	},
	
	addTagsToTree: function(tags) {
		if (tags.length > 0) {
			var childToSelect = undefined;
			
			var docsToExpand = [];
			
			var root = this.getRootNode();
			
			var docId = tags[0].docId; // TODO
//			var doc = root.findChild('docId', docId);
//			// collapse so we're not doing excess rendering
//			doc.collapse();
			
			var tag;
			for (var i = 0; i < tags.length; i++) {
				tag = tags[i];
				
				var docId = tag.docId;
				var doc = root.findChild('docId', docId);
				if (doc != null) {
					doc.collapse();
					if (docsToExpand.indexOf(doc) === -1) {
						docsToExpand.push(doc);
					}
					
					var text = tag.text || '';
					var nodeText = '<span class="dtc-tag-highlight">'+tag.label+'</span><br/>';
					if (tag.prevText) {
						nodeText += tag.prevText+'<span class="dtc-tag-highlight">'+text+'</span>'+tag.nextText;
					} else {
						nodeText += text;
					}
					
					var nodeConfig = {
						text: nodeText,
						cls: 'tag child',
						leaf: true,
						editable: false,
						draggable: false,
						identifier: tag.identifier,
						docId: tag.docId,
						tokenId: tag.tokenId,
						sortId: parseInt(tag.tokenId.split('_')[1]),
						dtcType: 'tag',
						tagData: tag
					};
					
					var child = doc.appendChild(nodeConfig);
					if (tag.docId === this._controller.docId && childToSelect === undefined) {
						childToSelect = child.id;
					}
				}
			}
			
			for (var i = 0; i < docsToExpand.length; i++) {
				docsToExpand[i].expand();
			}

			this.sortTree();
			
			if (childToSelect !== undefined) {
				this.setSelection(this.getStore().findRecord('id', childToSelect));
			}
			
//			doc.ui.addClass('hasChildren');
//			doc.expand();
		}
	},
	
	addIndexesToTree: function(indexes) {
		var root = this.getRootNode();
		var index, doc;
		var childToSelect = undefined;
		for (var i = 0; i < indexes.length; i++) {
			index = indexes[i];
			
			var text = index.text || '';
			if (index.prevText) {
				text = '<span class="dtc-index-highlight">'+index.label+'</span><br/>'+index.prevText+'<span class="dtc-index-highlight">'+text+'</span>'+index.nextText;
			} else {
				text = '<span class="dtc-index-highlight">'+index.label+'</span><br/>'+text;
			}
			
			doc = root.findChild('docId', index.docId);
			if (doc != null) {
				var child = doc.appendChild({
					text: text,
					cls: 'index child',
					leaf: true,
					editable: false,
					draggable: false,
					tokenId: index.tokenId,
					sortId: parseInt(index.tokenId.split('_')[1]),
					docId: index.docId,
					dtcType: 'index',
					indexData: index
				});
				if (index.docId === this._controller.docId && childToSelect === undefined) {
					childToSelect = child.id;
				}
	//			doc.ui.addClass('hasChildren');
				doc.expand();
			}
		}

		this.sortTree();
		
		if (childToSelect !== undefined) {
			this.setSelection(this.getStore().findRecord('id', childToSelect));
		}
	},
	
	addXPathToTree: function(docId, xpath) {
		var result = Ext.getCmp('dtcReader').getResultsForXPath(docId, xpath);
		if (result) {
			if (result.resultType == XPathResult.UNORDERED_NODE_ITERATOR_TYPE) {
				var firstChild;
				var root = this.getRootNode();
				var doc = root.findChild('docId', docId);
				if (doc != null) {
	//				doc.ui.removeClass('hasChildren');
					doc.removeAll(true);
					var tag = result.iterateNext();
					if (tag == null) {
						Ext.MessageBox.show({
							buttons: Ext.Msg.OK,
							icon: Ext.Msg.INFO,
							width: 300,
							height: 150,
							title: 'Info',
							msg: 'There were no matches for the supplied XPath.'
						});
						return;
					}
					while (tag) {
						var tokens = Ext.DomQuery.jsSelect('span[tokenId]', tag);
						var length = Math.min(tokens.length, 14);
						var text = '<span class="dtc-highlight">'+tag.nodeName+'</span>';
						for (var i = 0; i < length; i++) {
							text += ' '+tokens[i].textContent;
						}
						var tokenId = tag.getAttribute('tokenId');
						var child = doc.appendChild({
							text: text,
							leaf: true,
							expandable: false,
							editable: false,
							draggable: false,
							tokenId: tokenId,
							sortId: parseInt(tokenId.split('_')[1]),
							docId: docId,
							dtcType: 'xpath',
							listeners: {
								click: function(node, event) {
									var data = {
										xpath: "//span[@tokenId='"+node.getData().tokenId+"']",
										docId: node.getData().docId
									};
									this._controller.publish('tagSelected', data);
								},
								scope: this
							}
						});
						if (firstChild === undefined) {
							firstChild = child;
						}
						tag = result.iterateNext();
					}
	//				doc.ui.addClass('hasChildren');
					doc.expand();
				}
			} else {
				
			}

			this.sortTree();
			
			if (firstChild !== undefined) {
				this.setSelection(this.getStore().findRecord('id', firstChild.id));
			}
			
			Ext.getCmp('dtcMarkup').showHitsForTags();
		}
	},

	sortTree: function() {
		var sorter = function(r1, r2) {
			var s1 = r1.get('sortId');
			var s2 = r2.get('sortId');
			if (s1 === undefined || s2 === undefined) {
				return 0;
			}
			return s1 > s2 ? 1 : (s1 === s2) ? 0 : -1;
		}
		this.getRootNode().sort(sorter, true);
	},
	
	clearTree: function() {
		var root = this.getRootNode();
		root.eachChild(function(node) {
			node.removeAll();
			node.collapse();
			node.set('leaf', true);
		});
		
		this.down('#search').setValue('');
	},
	
	removeNodes: function(type, idsToKeep) {
		function doTest(r) {
			if (r.get('dtcType') === type && idsToKeep[r.getData().identifier] !== true) {
				nodesToRemove.push(r);
			}
		}
		
		idsToKeep = idsToKeep === undefined ? {} : idsToKeep;
		
		var store = this.getStore();
		var nodesToRemove = [];
		store.each(doTest);
		
		if (nodesToRemove.length > 0) {
			this.unbindStore(); // don't refresh while removing
			for (var i = 0; i < nodesToRemove.length; i++) {
				var node = nodesToRemove[i];
				node.erase();
			}
			this.bindStore(store);
		}
	},
	
	getChildrenForDocument: function(docId) {
		var children = [];
		var root = this.getRootNode();
		var docChild = root.findChild('docId', docId);
		if (docChild) {
			var childNodes = docChild.childNodes;
			for (var i = 0, len = childNodes.length; i < len; i++) {
				children.push(childNodes[i].getData());
			}
		}
		return children;
	},
	
	/*
	isXPathValid: function(xpath) {
		try {
			var result = document.evaluate(xpath, document, null, XPathResult.ANY_TYPE, null);
		} catch (e) {
			return false;
		}
		return true;
	},
	
	showXPathDialog: function(toolEl) {
		if (this.xpathWin == null) {
			this.xpathWin = Ext.create('Ext.window.Window', {
				modal: true,
				title: 'Add XPath',
				width: 300,
				layout: 'form',
				labelAlign: 'right',
				labelWidth: 75,
				items: [{
					xtype: 'textfield',
					itemId: 'xpath',
					fieldLabel: 'XPath'
				},{
					xtype: 'radiogroup',
					itemId: 'apply',
					fieldLabel: 'Add To',
					items: [{
						boxLabel: 'Current Doc',
						name: 'xpathApply',
						checked: true
					},{
						boxLabel: 'All Docs',
						name: 'xpathApply',
						checked: false,
						disabled: true
					}]
				}],
				buttons: [{
					text: 'Ok',
					handler: function() {
						var xpath = this.xpathWin.getComponent('xpath').getValue();
						var isValid = this.isXPathValid(xpath);
						if (!isValid) {
							Ext.MessageBox.show({
								buttons: Ext.Msg.OK,
								icon: Ext.Msg.ERROR,
								width: 300,
								height: 150,
								title: 'Error',
								msg: 'The supplied XPath is not valid.'
							});
						}
						var apply = this.xpathWin.getComponent('apply').getValue();
						if (apply.boxLabel == 'Current Doc') {
							var docId = Ext.getCmp('dtcReader').getCurrentDocId();
							this.addXPathToTree(docId, xpath);
						} else {
							
						}
						this.xpathWin.hide();
					},
					scope: this
				},{
					text: 'Cancel',
					handler: function() {
						this.xpathWin.hide();
					},
					scope: this
				}]
			});
		}
		this.xpathWin.show(toolEl);
	},*/
	
	getDocIdsOrdering: function() {
		return this.getRootNode().childNodes.map(child => child.get('docId'));
	}
	
});