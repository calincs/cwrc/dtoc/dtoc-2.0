import DToCPanel from '../../app/panel.js';

Ext.define("Ext.ux.IFrame",{
	extend: "Ext.Component",
	alias: "widget.uxiframe",
	loadMask: "Loading...",
	src: "about: blank",
	renderTpl: ['<iframe src="{src}" id="{id}-iframeEl" data-ref="iframeEl" name="{frameName}" width="100%" height="100%" frameborder="0"></iframe>'],
	childEls: ["iframeEl"],
	initComponent: function() {
		this.callParent();
		this.frameName=this.frameName||this.id+"-frame";
	},
	initEvents: function() {
		var a=this;
		a.callParent();
		a.iframeEl.on("load",a.onLoad,a);
	},
	initRenderData: function() {
		return Ext.apply(this.callParent(),{src: this.src,frameName: this.frameName})
	},
	getBody: function() {
		var a=this.getDoc();
		return a.body||a.documentElement;
	},
	getDoc: function() {
		try {
			return this.getWin().document;
		} catch(a) {
			return null;
		}
	},
	getWin: function() {
		var b=this,a=b.frameName,c=Ext.isIE?b.iframeEl.dom.contentWindow: window.frames[a];
		return c;
	},
	getFrame: function() {
		var a=this;
		return a.iframeEl.dom;
	},
	onLoad: function() {
		var a=this,b=a.getDoc();
		if (b) {
			this.el.unmask();
			this.fireEvent("load",this);
		} else {
			if (a.src) {
				this.el.unmask();
				this.fireEvent("error",this);
			}
		}
	},
	load: function(d) {
		var a=this,c=a.loadMask,b=a.getFrame();
		if (a.fireEvent("beforeload",a,d)!==false) {
			if (c&&a.el) {
				a.el.mask(c);
			}
			b.src=a.src=(d||a.src);
		}
	}
});

export default Ext.define('ReaderView', {
	extend: 'Ext.panel.Panel',
	mixins: ['DToC.Panel'],
	requires: ['Ext.ux.IFrame'],
	alias: 'widget.dtocReader',
	
	toolTipConfig: {
		cls: 'dtcReaderNote',
		showDelay: 50,
		hideDelay: 200,
		draggable: true,
		constrainPosition: true,
		border: false,
		shadow: false,
		padding: 5,
		maxWidth: 400,
		maxHeight: 420,
		scrollable: 'y'
	},

	loadMask: null,
	readerContainer: null,
	prevButton: null,
	nextButton: null,
	
	constructor: function(config) {
		this._controller = config._controller;

		this.urlBase = this._controller._app.urlBase;

		Ext.applyIf(config, {
			id: 'dtcReader',
			cls: 'dtc-panel',
			height: '100%',
			layout: 'fit',
			title: '',
			flex: 1,
			items: [{
				id: 'dtcReaderContainer',
				xtype: 'uxiframe',
				hidden: true,
				src: this.urlBase+'data/xml/blank.xml'
			}]
		});

		if (config.showMenu !== false) {
			config.tools = [{
				glyph: 'xf0c9@FontAwesome',
				margin: '0 10 0 0',
				padding: '0 5 0 10',
				menu: undefined,
				callback: function(panel, btn, event) {
					if (btn.menu === undefined) {
						btn.menu = Ext.create('Ext.menu.Menu', {
							plain: true,
							renderTo: panel.getEl(),
							items: [{
								text: 'Edit Configuration',
								glyph: 'xf304@FontAwesome',
								handler: function(item, event) {
									this._controller._app.showInputWindow(this._controller._app.getInputConfig());
								},
								scope: this
							},{
								text: 'Enter Curator Mode',
								glyph: 'xf02b@FontAwesome',
								currentMode: 'default',
								handler: function(item, event) {
									if (item.currentMode === 'default') {
										this._controller._app.showEditView();
										item.currentMode = 'edit';
										item.setGlyph('xf00c@FontAwesome');
										item.setText('Done Curation');
									} else {
										this._controller._app.showDefaultView();
										item.currentMode = 'default';
										item.setGlyph('xf02b@FontAwesome');
										item.setText('Enter Curator Mode');
									}
								},
								scope: this
							},'-',{
								text: 'Help',
								glyph: 'xf128@FontAwesome',
								handler: function() {
									this._controller._app.showHelp();
								},
								scope: this
							}]
						});
					}
					btn.menu.showAt(event.getXY());
				},
				scope: this
			}];
		}
		
		this.callParent([config]);
		this.mixins['DToC.Panel'].constructor.apply(this, [config]);
		
		this.addListener('afterrender', function(panel) {
			this.readerContainer = Ext.getCmp('dtcReaderContainer');
		}, this);
		
		this.addListener('afterlayout', function(panel, layout) {
			this.resizeReaderComponents();
		}, this);
		
		this._doScrollTo = function(amount, animate) {
			var dom = this.readerContainer.getBody();
			var scrollTop = amount * dom.scrollHeight - (dom.clientHeight * 0.5);
			Ext.fly(this.readerContainer.getBody()).scrollTo('top', scrollTop, animate);
		};
	},
	initComponent: function() {
		var me = this;
		
		me.callParent(arguments);
	},
	
	setText: function(xmlStr, cssUrl) {
		return new Promise((resolve, reject) => {
			this.readerContainer.on('load', async function() {
				this.readerContainer.show();

				this.readerContainer.getDoc().firstElementChild.innerHTML = xmlStr;

				await this._controller.addCustomStylesheetToReader();
				await this._controller.addStylesheetToReader(cssUrl);

				var doc = this.readerContainer.getDoc();
				// need to add scroll every time doc is changed
				doc.addEventListener('scroll', function(ev) {
					this._controller.publish('readerScroll', {el: ev.target.firstElementChild, docId: this._controller.docId});
				}.bind(this));
				
				this._controller._processLinks(doc);
				this._controller._processNotes();
				this._controller._processImages();

				this._controller._highlightDocSelections();
				
				this.loadMask.animate({
					duration: 500,
					to: { opacity: 0 },
					listeners: {
						afteranimate: function() {
							this.loadMask.hide();
							this.loadMask.getEl().setOpacity(1);
						},
						scope: this
					}
				});

				this._controller.publish('documentLoaded', {docId:this._controller.docId});
				
				setTimeout(resolve, 150); // delay so that css has a chance to be applied (needed for scrollToToken)
			}, this, {single: true});

			if (this.loadMask === null) {
				this.loadMask = new Ext.LoadMask({
					msg: 'Loading',
					target: this.readerContainer,
					style: {
						backgroundColor: 'rgba(255, 255, 255, 1)'
					}
				});
			}
			this.loadMask.show();
			this.readerContainer.load(this.urlBase+'data/xml/blank.xml');

		});
	},
	
	resizeReaderComponents: function() {
		var parentWidth = this.readerContainer.getEl().getWidth();
		this.readerContainer.getEl().setWidth(parentWidth);
	},
	
	setReaderScroll: function(location) {
		location = location || 'bottom';
		var scrollTop = 0;
		if (location == 'bottom') {
			scrollTop = this.readerContainer.getBody().scrollHeight;
		}
		Ext.fly(this.readerContainer.getBody()).scrollTo('top', scrollTop, false);
	}
});

Ext.override(Ext.Element, {
	scrollIntoView: function(container, hscroll) {
		// use native api to scroll element into the centre of the container
		this.dom.scrollIntoView({behavior: 'auto', block: 'center', inline: 'center'});
	},
	
	frame: function(color, count, obj){
		var me = this,
			dom = me.dom,
			beforeAnim;

		color = color || '#C3DAF9';
		count = count || 1;
		obj = obj || {};

		beforeAnim = function() {
			var el = Ext.fly(dom, '_anim'),
				animScope = this,
				box,
				proxy, proxyAnim;
				
			// OVERRIDE xml elements have no style property
			if (dom.style !== undefined) {
				el.show();
			}
			
			box = el.getBox();

			// OVERRIDE add iframe offset to box
			if (el.dom.ownerDocument !== window.document) {
				var iframeBox = Ext.fly(window.document.querySelector('iframe')).getBox();
				box.x += iframeBox.x;
				box.y += iframeBox.y;
			}

			proxy = Ext.getBody().createChild({
				role: 'presentation',
				id: el.dom.id + '-anim-proxy',
				style: {
					position : 'absolute',
					'pointer-events': 'none',
					'z-index': 35000,
					border : '0px solid ' + color
				}
			});
			
			proxyAnim = new Ext.fx.Anim({
				target: proxy,
				duration: obj.duration || 1000,
				iterations: count,
				from: {
					top: box.y,
					left: box.x,
					borderWidth: 0,
					opacity: 1,
					height: box.height,
					width: box.width
				},
				to: {
					top: box.y - 20,
					left: box.x - 20,
					borderWidth: 10,
					opacity: 0,
					height: box.height + 40,
					width: box.width + 40
				}
			});
			proxyAnim.on('afteranimate', function() {
				proxy.destroy();
				// kill the no-op element animation created below 
				animScope.end();
			});
		};

		me.animate({
			// See "A Note About Wrapped Animations" at the top of this class: 
			duration: (Math.max(obj.duration, 500) * 2) || 2000,
			listeners: {
				beforeanimate: {
					fn: beforeAnim
				}
			},
			callback: obj.callback,
			scope: obj.scope
		});
		return me;
	}
});