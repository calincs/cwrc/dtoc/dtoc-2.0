import DocLoader from '../../app/docloader.js';

// all the tag loading and parsing logic happens here

/**
 * The metadata for an xpath
 * @typedef {Object} TagMetadata
 * @prop {String} docId The document ID
 * @prop {String} label The label for the xpath (shown to user)
 * @prop {String} xpath The xpath
 * @prop {String} text A snippet of text from within the tag. Snippet length controlled by TAG_SNIPPET_WORD_LENGTH
 * @prop {String} [prevText] A snippet of text preceding the tag
 * @prop {String} [nextText] A snippet of text proceding the tag
 * @prop {String} tokenId The token ID corresponding to the tag
 * @prop {String} type The type: 't' for tag, 'x' for xpath
 */

/**
 * Contains all the tag metadata for this document. Each map key is the xpath.
 * @typedef {Object<String, Array<TagMetadata>} DocumentTagMetadata
 */

/**
 * Contains all the tag metadata for the corpus. Each map key is the document ID.
 * @typedef {Object<String, Array<DocumentTagMetadata>} CorpusTagMetadata
 */

/**
 * Curated tags map. Each map key is the xpath.
 * The map values are essentially TagMetadata instances but with only xpath, label, and type properties.
 * @typedef {Object<String, TagMetadata>} CuratedTagMap
 */

/**
 * A class containing static methods for parsing tags and producing tag metdata.
 */
export default class MarkupProcessor {
	
	static TAG_SNIPPET_WORD_LENGTH = 10;
	static ignoredPrefixes = ['rdf','cw']; // ignore tags with these prefixes (cw used by cwrc-writer docs)
	
	/**
	 * Get the metadata for the curatedTags. If no curation is specified, gets all tags.
	 * @param {Corpus} corpus The corpus
	 * @param {CuratedTagMap} [curatedTags] The curated tags to process
	 * @returns {Promise} A promise that resolves with the tag metadata
	 */
	static processTags(corpus, curatedTags) {
		return new Promise((resolve, reject) => {
			let corpusTagMetadata = {};

			let currDocIndex = 0;
			function doLoad(index) {
				if (index < corpus.getDocumentsCount()) {
					currDocIndex++;
					const doc = corpus.getDocument(index);
					// check to see if this doc was specified as the index (via the cwrc interface)
					if (doc.isDtocIndex === true) {
						doLoad(currDocIndex);
					} else {
						const xmlDoc = corpus.getDocument(doc.id).xml;
						const tagData = MarkupProcessor._parseTags(xmlDoc, doc.id, curatedTags);
						corpusTagMetadata[doc.id] = tagData;
						doLoad(currDocIndex);
					}
				} else {
					resolve(corpusTagMetadata);
				}
			}

			doLoad(currDocIndex);
		});
	}

	static getText(tag) {
		const shortRe = new RegExp('(([^\\s]+\\s\\s*){'+MarkupProcessor.TAG_SNIPPET_WORD_LENGTH+'})(.*)'); // get first X words
		let text = tag.textContent;
		text = text.replace(/\s+/g, ' '); // consolidate whitespace
		const shortText = text.replace(shortRe, "$1");
		return {content: shortText, shortened: shortText.length < text.length};
	}

	static getSurroundingText(tag) {
		var prevRe = new RegExp('(.*)(\\s([^\\s]+\\s\\s*){'+Math.floor(MarkupProcessor.TAG_SNIPPET_WORD_LENGTH/2)+'})'); // get last X words
		var nextRe = new RegExp('(([^\\s]+\\s\\s*){'+Math.floor(MarkupProcessor.TAG_SNIPPET_WORD_LENGTH/2)+'})(.*)');
		
		function doGet(currTag, dir, currText) {
			var walker = currTag.ownerDocument.createTreeWalker(currTag.ownerDocument, NodeFilter.SHOW_TEXT, null, false);
			walker.currentNode = currTag;
			if (dir === 'prev') {
				walker.previousNode();
			} else {
				walker.nextNode();
			}
			var node = walker.currentNode;
			if (node != null) {
				var text = node.textContent;
				if (dir === 'prev') {
					currText = text + currText;
				} else {
					currText += text;
				}
				if (currText.length > 30) return currText;
				else return doGet(node, dir, currText);
			} else {
				return currText;
			}
		}
		
		var prevText = doGet(tag, 'prev', '');
		var match = prevText.match(prevRe);
		if (match != null) {
			prevText = match[2];
		}
		var walker = tag.ownerDocument.createTreeWalker(tag.ownerDocument, NodeFilter.SHOW_ALL, null, false);
		walker.currentNode = tag;
		walker.nextSibling();
		tag = walker.currentNode;
		var currText = '';
		if (tag.nodeType === Node.TEXT_NODE) {
			currText = tag.textContent;
		}
		var nextText = doGet(tag, 'next', currText);
		match = nextText.match(nextRe);
		if (match != null) {
			nextText = match[1];
		}
		
		return [prevText, nextText];
	}
	
	/**
	 * Parses the tags in an xml document and gathers metadata.
	 * @param {Document} xml The xml document.
	 * @param {String} docId The id for the document.
	 * @param {Object} [customTagSet] An object containing a set of tags to look for, in the same format as curatedTags
	 * @returns {Object} The tag metadata.
	 */
	static _parseTags(xml, docId, customTagSet) {
		var docBody = xml.documentElement || xml;
		if (docBody == null) {
			if (window.console) {
				console.warn('parse error');
			}
			return {};
		}
		
		function produceTagData(xpath, label) {
			const data = {};

			let results = [];
			try {
				results = DocLoader.evaluateXPath(xpath, xml);
			} catch (e) {
				console.warn('Markup._parseTags: error evaluating xpath', xpath, e.message);
			}

			for (const element of results) {
				
				if (MarkupProcessor.ignoredPrefixes.indexOf(element.prefix) !== -1) continue;
				
				const tokenId = element.getAttribute('tokenId');
				
				if (tokenId === null) {
					// empty elements lack tokenId attribute
				} else {
					const dataObj = {
						docId: docId,
						tokenId: tokenId,
						label: label || element.nodeName
					};

					const text = MarkupProcessor.getText(element);
					dataObj.text = text.content;
					if (text.shortened === false) {
						const surrText = MarkupProcessor.getSurroundingText(element);
						dataObj.prevText = surrText[0];
						dataObj.nextText = surrText[1];
					} else {
						dataObj.text += '&hellip;';
					}

					let xpathOrNodeName = xpath;
					if (xpath === '*') {
						xpathOrNodeName = '//'+element.nodeName;
					}
					dataObj.xpath = xpathOrNodeName;
					
					if (data[xpathOrNodeName] == null) {
						data[xpathOrNodeName] = [dataObj];
					} else {
						data[xpathOrNodeName].push(dataObj);
					}
				}
			}

			return data;
		}
		
		let returnData = {};
		if (customTagSet === undefined || Object.entries(customTagSet).length === 0) {
			// no curation so parse all tags
			returnData = produceTagData.call(this, '*');
		} else {
			// find hits for curated tags only
			for (const tag in customTagSet) {
				const cTag = customTagSet[tag];
				const data = produceTagData.call(this, cTag.xpath, cTag.label);
				Object.assign(returnData, data);
			}
		}
		
		// process header
		var head = docBody.querySelectorAll('head').item(0);
		if (head) {
			// special handling for TEI docs
			// OLD TODO save in stored resource
			var authors = head.querySelectorAll('author');
			if (authors.length > 0) {
				var authorsArray = [];
				for (var i = 0; i < authors.length; i++) {
					var author = authors.item(i);
					var authorObj = {};
					var updateAuthor = false;
					
					var forename = author.getElementsByTagName('forename').item(0);
					var surname = author.getElementsByTagName('surname').item(0);
					if (forename !== null) {
						authorObj.forename = forename.textContent;
						updateAuthor = true;
					}
					if (surname !== null) {
						authorObj.surname = surname.textContent;
						updateAuthor = true;
					}
					
					if (updateAuthor) {
						authorsArray.push(authorObj);
					}
				}
				if (authorsArray.length > 0) {
					// TODO check authors array handling
					this.getCorpus().getDocument(docId).author = authorsArray;
				}
			}
		}
		
		return returnData;
	}	
}
