import Component from '../component.js';
import MarkupView from './view.js';
import MarkupProcessor from './processor.js';
import MarkupViewCurator from './view.curator.js';


export default class Markup extends Component {
	/**
	 * Metadata for tags. Each map key is a document ID.
	 * @type {MarkupProcessor~CorpusTagMetadata}
	 */
	tagMetadata = {};

	/**
	 * The map of curated tags to load.
	 * @type {MarkupProcessor~CuratedTagMap}
	 */
	curatedTags = {};

	/**
	 * Tracks tag freq counts for entire corpus
	 * @type {Object}
	 */
	tagTotals = {};

	constructor(app) {
		super(app);
		this.view = new MarkupView({_controller: this});

		this.subscribe('loadedCorpus', function(src, corpus) {
			this.view.updateChapterFilter(corpus);

			if (this._app.useIndex === false) {
				this.loadAllTags(true);	
			}
		}, this);
		this.subscribe('indexProcessed', function() {
			this.loadAllTags(false);
		}, this);
		this.subscribe('chapterFilterSelected', function(src, docId) {
			// TODO need to make sure src isn't us
			this.view.doChapterFilter(docId, false);
		}, this);
		this.subscribe('clearSelections', function(src, data) {
			this.view.clearSelections();
		}, this);
	}

	reset() {
		this.view.store.clearFilter(true);
		this.view.store.removeAll();
		this.tagMetadata = {};
		this.curatedTags = {};
		this.tagTotals = {};
	}

	showEditView() {
		const tagData = this.getExportData();

		const container = this.view.up();
		container.remove(this.view);

		this.view = new MarkupViewCurator({_controller: this});
		this.view.store.loadData(tagData, false);

		container.insert(1, this.view);
		container.setActiveTab(this.view);
		container.setWidth(500);
	}

	showDefaultView() {
		const tagData = this.getExportData();

		const container = this.view.up();
		container.remove(this.view);
		
		this.view = new MarkupView({_controller: this});
		this.curatedTags = {};
		this.tagTotals = {};
		this.tagMetadata = {};
		for (let i = 0; i < tagData.length; i++) {
			const tag = tagData[i];
			this.curatedTags[tag.xpath] = tag;
			if (tag.usage) {
				if (tag.usage === 'link') {
					this._app.reader.linkSelectors.push(tag.xpath);
				} else if (tag.usage === 'note') {
					this._app.reader.noteSelectors.push(tag.xpath);
				} else if (tag.usage === 'image') {
					this._app.reader.imageSelectors.push(tag.xpath);
				}
			}
		}

		container.insert(1, this.view);
		container.setActiveTab(this.view);
		container.setWidth(310);

		this.loadAllTags(true);
		this.view.updateChapterFilter(this.getCorpus());

		this._app.reader.reloadDocument();
	}

	async loadAllTags(useMask) {
		if (this.getCorpus() === undefined) {
			throw new Error('loadAllTags called with no corpus!');
		}
		if (useMask && this.view.rendered) {
			this.view._maskEl = this.view.body.mask('Processing Tags', 'loadMask');
		}

		this.tagMetadata = await MarkupProcessor.processTags(this.getCorpus(), this.curatedTags);
		
		// calculate tagTotals
		for (let docId in this.tagMetadata) {
			const docData = this.tagMetadata[docId];
			for (let xpath in docData) {
				const tagData = docData[xpath];
				if (tagData != null) {
					if (this.tagTotals[xpath] == null) {
						this.tagTotals[xpath] = {
							freq: tagData.length,
							label: tagData[0].label,
							type: tagData[0].type
						};
					} else {
						this.tagTotals[xpath].freq += tagData.length;
					}
				}
			};
		}

		const tagDataWithTotals = [];
		for (let tag in this.tagTotals) {
			const tagTotalsEntry = this.tagTotals[tag];
			const freq = tagTotalsEntry.freq || 1;
			tagDataWithTotals.push({
				xpath: tag,
				label: tagTotalsEntry.label || tag,
				freq: freq
			});
		}
		this.view.store.loadData(tagDataWithTotals, true);
		this.view.store.sort('label', 'ASC');
		this.publish('allTagsLoaded', tagDataWithTotals);

		if (useMask && this.view._maskEl !== null) {
			this.view.body.unmask();
			this.view._maskEl = null;
		}
	}

	getExportData() {
		const jsonData = [];
		this.view.store.each(function(rec) {
			const data = rec.data;
			if (data.xpath !== '') {
				if (data.label === '') {
					data.label = data.xpath;
				}
				delete data.freq;
				delete data.id;
				if (data.usage) {
					if (data.usage === null) {
						delete data.usage;
					}
				} else if (this.curatedTags !== null) {
					// need to pull in curatedTag usage info when moving from default to curator mode
					const curatedTag = this.curatedTags[data.xpath];
					if (curatedTag && curatedTag.usage) {
						data.usage = curatedTag.usage;
					}
				}
				jsonData.push(data);
			}
		}, this);
		return jsonData;
	}

}