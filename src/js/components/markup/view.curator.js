import MarkupProcessor from "./processor";

export default Ext.define('MarkupViewCurator', {
	extend: 'Ext.grid.Panel',
	
	disabledTags: {},
	
	
	constructor: function(config) {
		this._controller = config._controller;

		this.callParent(arguments);
	},
	
	initComponent: function() {
		var tagStore = Ext.create('Ext.data.JsonStore', {
			fields: [
				{name: 'xpath', allowBlank: false},
				{name: 'label', allowBlank: false},
				{name: 'usage'}
			],
			sortInfo: {field: 'label', direction: 'ASC'},
			data: [],
			listeners: {
				datachanged: function(store) {
					for (var i = 0, len = store.getCount(); i < len; i++) {
						var r = store.getAt(i);
						if (r.get('type') === 't') {
							this.disabledTags[r.get('xpath')] = true;
						}
					}
				},
				scope: this
			}
		});
		
		this.tagCombo = Ext.create('Ext.form.field.ComboBox', {
			xtype: 'combo',
			triggerAction: 'all',
			queryMode: 'local',
			editable: true,
			allowBlank: false,
			autoSelect: false,
			store: new Ext.data.ArrayStore({
				idIndex: 0,
				fields: ['tag', 'disabled']
			}),
			tpl: '<tpl for=".">'+
					'<tpl if="disabled == false">'+
						'<li role="option" unselectable="on" class="x-boundlist-item">{tag}</li>'+
					'</tpl>'+
					'<tpl if="disabled == true">'+
					'<li role="option" unselectable="on" class="x-boundlist-item disabled">{tag}</li>'+
					'</tpl>'+
				'</tpl>',
			valueField: 'tag',
			displayField: 'tag',
			listeners: {
				beforeselect: function(combo, record, index) {
					return !record.get('disabled');
				},
				scope: this
			}
		});
		
		this.usageCombo = Ext.create('Ext.form.field.ComboBox', {
			xtype: 'combo',
			triggerAction: 'all',
			queryMode: 'local',
			editable: true,
			allowBlank: true,
			autoSelect: false,
			forceSelection: true,
			store: new Ext.data.ArrayStore({
				idIndex: 0,
				fields: ['type', 'label'],
				data: [['image', 'Image'], ['note', 'Note'], ['link', 'Link']]
			}),
			valueField: 'type',
			displayField: 'label'
		})
		
		Ext.apply(this, {
			title: 'Tags Editor',
			plugins: {
				ptype: 'cellediting',
				pluginId: 'cellEditor'
			},
			store: tagStore,
			forceFit: true,
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				enableOverflow: true,
				items: [{
					xtype: 'button',
					text: 'Add',
					glyph: 'xf067@FontAwesome',
					handler: function(b) {
						this.store.loadData([{
							xpath: '',
							label: '',
							freq: 0
						}], true);
						var numRecords = this.store.getCount();
						this.getPlugin('cellEditor').startEditByPosition({row: numRecords-1, column: 0});
					},
					scope: this
				},' ',{
					xtype: 'button',
					text: 'Remove',
					glyph: 'xf068@FontAwesome',
					handler: this.doRemoveSelections,
					scope: this
				},' ',{
					xtype: 'button',
					text: 'Show',
					glyph: 'xf002@FontAwesome',
					handler: this.doShowSelections,
					scope: this
				}]
			}],
			viewConfig: {
				stripeRows: true
			},
			selModel: Ext.create('Ext.selection.RowModel', {
				mode: 'MULTI'
			}),
			columns:[{
				header: 'XPath', dataIndex: 'xpath',
				editor: this.tagCombo
			},{
				header: 'Label', dataIndex: 'label',
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}
			},{
				header: 'Usage', dataIndex: 'usage',
				editor: this.usageCombo
			}],
			listeners: {
				edit: function(editor, e) {
					if (e.field === 'xpath') {
						// TODO
						var comboStore = this.tagCombo.getStore();
						var r = comboStore.findRecord('tag', e.value);
						if (r !== null) r.set('disabled', true);
						r = comboStore.findRecord('tag', e.originalValue);
						if (r !== null) r.set('disabled', false);
					}
				},
				scope: this
			}
		});
		
		this.getAllTags();
		
		this.callParent(arguments);
	},
	
	getAllTags: function() {
		var me = this;
		var corpus = me._controller.getCorpus();
		var currDocIndex = 0;
		var allTagsHashMap = {};
		
		function getDocument(index) {
			if (index < corpus.getDocumentsCount()) {
				currDocIndex++;
				
				var docXML = corpus.getDocument(index).xml;
				var tags = Ext.dom.Query.select('*', docXML.firstElementChild);
				for (var i = 0; i < tags.length; i++) {
					var tag = tags[i];
					var nodeName = tag.nodeName;
					allTagsHashMap[nodeName] = true;
				}

				getDocument(currDocIndex);
			} else {
				var data = [];
				for (var tag in allTagsHashMap) {
					data.push([tag, me.disabledTags[tag] != null]);
				}
				data.sort();
				me.tagCombo.getStore().loadData(data, false);
			}
			
		}
		
		getDocument(currDocIndex);
	},
	
	doRemoveSelections: function() {
		var selections = this.getSelection();
		if (selections.length > 0) {
			Ext.Msg.confirm('Remove Selections', 'Do you really want to remove the selected entries?',
				function(button) {
					if (button === 'yes') {
						var comboStore = this.tagCombo.getStore();
						for (var i = 0; i < selections.length; i++) {
							var xpath = selections[i].get('xpath');
							delete this.disabledTags[xpath];
							var r = comboStore.findRecord('tag', xpath);
							if (r !== null) {
								r.set('disabled', false);
							}
						}
						this.store.remove(selections);
					}
				}.bind(this)
			);
		} else {
			Ext.Msg.show({
				title: 'No Selections',
				message: 'You must select at least one entry to remove.',
				buttons: Ext.Msg.OK,
				icon: Ext.Msg.INFO
			});
		}
	},
	
	doShowSelections: function(selections) {
		var selections = this.getSelection();
		
		if (selections.length > 0) {
			var waitForHits = false;
			var waitTracker = 0;
			var newTags = {};
			var tags = [];
			
			function doDispatch(data) {
				for (var xpath in data) {
					var a = data[xpath];
					if (a != null) {
						tags = tags.concat([a]);
					}
				}
				waitTracker--;
				if (waitTracker == 0) {
					this.body.unmask();
					this._maskEl = null;
					this._controller.publish('tagsSelected', tags);
				}
			}
			
			for (var i = 0; i < selections.length; i++) {
				var sel = selections[i].data;
				for (var docId in this._controller.tagMetadata) {
					var tagData = this._controller.tagMetadata[docId][sel.xpath];
					if (tagData === undefined) {
						waitForHits = true;
						if (newTags[docId] == null) {
							newTags[docId] = [];
						}
						newTags[docId].push(sel);
					} else if (tagData !== null) {  // if it's null, then we've already checked this tag/xpath
						if (tagData[0] && tagData[0].label != sel.label) {
							// update label
							for (var j = 0; j < tagData.length; j++) {
								tagData[j].label = sel.label;
							}
						}
						tags.push(tagData);
					}
				}
			}
			if (!waitForHits) {
				this._controller.publish('tagsSelected', tags);
			} else {
				this._maskEl = this.body.mask('Fetching Selections', 'loadMask');
				for (var docId in newTags) {
					waitTracker++;
					this.getHitsForTags(newTags[docId], docId, doDispatch);
				}
			}
		} else {
			Ext.Msg.show({
				title: 'No Selections',
				message: 'You must select at least one entry to show.',
				buttons: Ext.Msg.OK,
				icon: Ext.Msg.INFO
			});
		}
	},
	
	getHitsForTags: function(tagArray, docId, callback) {
		var customTagSet = {};
		for (var i = 0; i < tagArray.length; i++) {
			var t = tagArray[i];
			customTagSet[t.xpath] = t;
		}

		var docXML = this._controller.getCorpus().getDocument(docId).xml;
		var tagData = MarkupProcessor._parseTags(docXML, docId, customTagSet);
//		console.debug(tagData);
		for (var xpath in tagData) {
			var t = tagData[xpath];
			this._controller.tagMetadata[docId][xpath] = t;
		}
		if (callback) callback.call(this, tagData);
	}

});