export default class Corpus {
	/**
	 * The Corpus class
	 * @param {Object} config
	 * @param {String} [config.id]
	 * @param {String} [config.title]
	 * @param {String} [config.subTitle]
	 * @param {Array} config.documents
	 */
	constructor({id = '', title = '', subTitle = '', documents}) {
		this._id = id;
		this._title = title;
		this._subTitle = subTitle;
		this._documents = documents;
	}

	get id() {
		return this._id;
	}
	set id(id) {
		this._id = id;
	}

	get title() {
		return this._title;
	}
	set title(title) {
		this._title = title;
	}
	
	get subTitle() {
		return this._subTitle;
	}
	set subTitle(subTitle) {
		this._subTitle = subTitle;
	}

	getDocumentsCount() {
		return this._documents.length;
	}

	getDocument(indexOrId) {
		let match = undefined;
		if (typeof indexOrId === 'string') {
			match = this._documents.find(doc => doc.id === indexOrId);
		} else {
			match = this._documents[indexOrId];
		}
		return match;
	}

	getDocumentIndex(docOrDocId) {
		let docId = docOrDocId;
		if (typeof docOrDocId !== 'string') {
			docId = docOrDocId.id || docOrDocId.docId;
		}
		for (let i = 0; i < this._documents.length; i++) {
			if (this._documents[i].id === docId) {
				return i;
			}
		}
		throw new Error('No document found for', docId);
	}

	get documents() {
		return this._documents;
	}

	/**
	 * Re-order the documents based on the array of docIds
	 * @param {Array} docIds An array of docIds
	 */
	setDocumentsOrder(docIds) {
		this._documents.sort((a, b) => {
			let indexA = docIds.indexOf(a.id);
			let indexB = docIds.indexOf(b.id);
			if (indexA === -1) indexA = this._documents.indexOf(a);
			if (indexB === -1) indexB = this._documents.indexOf(b);
			return indexA-indexB;
		})
	}
}
