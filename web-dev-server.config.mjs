export default {
    open: true,
    watch: true,
    port: 8888,
    rootDir: 'dev'
}